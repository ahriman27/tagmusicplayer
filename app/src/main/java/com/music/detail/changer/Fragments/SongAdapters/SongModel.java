package com.music.detail.changer.Fragments.SongAdapters;

public class SongModel {

    public final long id;
    public final String title;
    public final long albumId;
    public final String albumName;
    public final long artistId;
    public final String artistName;
    public final int duration;
    public final int trackNumber;
    private static boolean isSelected = false;

    public SongModel() {
        id = -1;
        title = "";
        albumId = -1;
        albumName = "";
        artistId = -1;
        artistName = "";
        duration = -1;
        trackNumber = -1;
    }

    public SongModel(long id, String title, long albumId, String albumName, long artistId, String artistName, int duration, int trackNumber) {
        this.id = id;
        this.title = title;
        this.albumId = albumId;
        this.albumName = albumName;
        this.artistId = artistId;
        this.artistName = artistName;
        this.duration = duration;
        this.trackNumber = trackNumber;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public long getAlbumId() {
        return albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public long getArtistId() {
        return artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public int getDuration() {
        return duration;
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public static boolean isSelected() {
        return isSelected;
    }

    public static void setSelected(boolean selected) {
        isSelected = selected;
    }
}
