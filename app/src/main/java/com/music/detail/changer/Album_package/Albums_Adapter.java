package com.music.detail.changer.Album_package;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.music.detail.changer.R;
import com.music.detail.changer.Song_Edits;

import java.util.ArrayList;

public class Albums_Adapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    public ArrayList albums_List;
    public ArrayList nested_Songs;
    public ArrayList cover_List;
    public ArrayList artistList;
    public ArrayList path_List;
    public ArrayList path_Rel_List;

    public Albums_Adapter(Context context, ArrayList arrayList, ArrayList Album_list, ArrayList cover, ArrayList artistList, ArrayList path_List, ArrayList path_Rel_List) {
        this.context = context;
        this.albums_List = arrayList;
        this.nested_Songs = Album_list;
        this.cover_List = cover;
        this.artistList = artistList;
        this.path_List = path_List;
        this.path_Rel_List = path_Rel_List;
    }

    @Override
    public int getCount() {
        return albums_List.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        }
        if (view == null) {
            view = inflater.inflate(R.layout.album_song_design, null);

        }
        LinearLayout linearLayout = view.findViewById(R.id.click_album_layout);
        TextView albumtextView = view.findViewById(R.id.album_name);
        ImageView imageView = view.findViewById(R.id.album_poster);
        TextView artistTectView = view.findViewById(R.id.artist_name);


        albumtextView.setText(albums_List.get(i) + "");
        artistTectView.setText(artistList.get(i) + "");


//            ImageLoader.getInstance().displayImage(getImage(Long.parseLong(cover_List.get(i).toString())).toString(),
//                        imageView, new DisplayImageOptions.Builder().cacheInMemory(true).showImageOnLoading(R.drawable.songs_icon).resetViewBeforeLoading(true)
//                                .build());
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), getImage(Long.parseLong(cover_List.get(i).toString())));

            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {

            imageView.setImageResource(R.drawable.album_icon);

        }


//        final ArrayList album_song_list=(ArrayList) album_list.get(i);
////        Toast.makeText(context, "Songs List"+album_song_list, Toast.LENGTH_SHORT).show();
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //          Toast.makeText(context, ""+nested_Songs.get(i), Toast.LENGTH_SHORT).show();

                Intent s = new Intent(context, Song_Edits.class);
                s.putExtra("songs", (ArrayList) nested_Songs.get(i));
                s.putExtra("paths", (ArrayList) path_List.get(i));
                s.putExtra("relpaths", (ArrayList) path_Rel_List.get(i));
                s.putExtra("cover", cover_List.get(i) + "");
                s.putExtra("album", albums_List.get(i) + "");
                s.putExtra("artist", artistList.get(i) + "");
                context.startActivity(s);

                //   Log.e("songs",(ArrayList)nested_Songs.get(i)+"");
                // Log.e("paths",(ArrayList)path_List.get(i)+"");
                // Log.e("relpaths",(ArrayList)path_Rel_List.get(i)+"");
                //Log.e("cover",cover_List.get(i)+"");
                //Log.e("album",albums_List.get(i)+"");
                //Log.e("artist",artistList.get(i)+"");

                ((Activity) context).finish();

            }
        });

        return view;
    }

    private Uri getImage(long albumId) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId);
    }

    public void setFilter(ArrayList filteredalbumList, ArrayList filteredartistList, ArrayList filteredcoverList, ArrayList filteredSongsList, ArrayList pathlist, ArrayList pathRellist) {
        this.albums_List = filteredalbumList;
        this.artistList = filteredartistList;
        this.cover_List = filteredcoverList;
        this.nested_Songs = filteredSongsList;
        this.path_List = pathlist;
        this.path_Rel_List = pathRellist;

        notifyDataSetChanged();
    }


}
