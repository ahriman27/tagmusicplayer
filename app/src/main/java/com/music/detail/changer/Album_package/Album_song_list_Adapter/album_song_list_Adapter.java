package com.music.detail.changer.Album_package.Album_song_list_Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.music.detail.changer.R;

import java.util.ArrayList;

public class album_song_list_Adapter extends RecyclerView.Adapter<album_song_list_Adapter.ViewHolder> {
    Context context;
    ArrayList arrayList;
    RecyclerView recyclerView;

    public album_song_list_Adapter(Context context, ArrayList al, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.context = context;
        this.arrayList = al;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.custom_songs_design, parent, false);
        ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
        // params.height=100*arrayList.size();
        //params.height=v.getHeight()*(arrayList.size());
        recyclerView.setLayoutParams(params);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(arrayList.get(position) + "");
//        holder.textView.setText("hiiii");
//        Toast.makeText(context, "data", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.song_name);
        }
    }
}
