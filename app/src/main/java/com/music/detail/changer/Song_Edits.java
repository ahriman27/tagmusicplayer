package com.music.detail.changer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.documentfile.provider.DocumentFile;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.images.Artwork;
import org.jaudiotagger.tag.images.ArtworkFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Song_Edits extends AppCompatActivity {
    AppCompatImageView appCompatImageView;
    TextView album_name, artist_name, toolbarAlbumName;
    LinearLayout mysonglayout, progresslayout;
    Toolbar toolbar;
    int myflag = 0;
    ImageView gifview;
    TextInputLayout til1, til2, til3, til4;
    Bitmap myLoadedBitmap;
    CoordinatorLayout outerLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    LinearLayout open_image_layout;
    String sdcardStorage;
    TextInputEditText et_album_name, et_album_artist, et_album_year, et_album_genre;
    ArrayList pathList, rel_path_list;
    MediaPlayer mediaPlayer;
    ImageView moreIconFromAlbum;
    int flag = 0;
    ProgressBar progressBar;
    AdView adView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song__edits);


        MobileAds.initialize(getApplicationContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adView = findViewById(R.id.albumEditPageAdview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(Song_Edits.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8305515768751170/6479165367");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        // getSupportActionBar().hide();

        // getUri();

        appCompatImageView = findViewById(R.id.album_appcompat_image);
        album_name = findViewById(R.id.album_name_edit);
        artist_name = findViewById(R.id.artist_name_edit);
        mysonglayout = findViewById(R.id.my_songs_layout);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_layout_album);
        appBarLayout = findViewById(R.id.song_app_bar);
        collapsingToolbarLayout.setEnabled(false);
        open_image_layout = findViewById(R.id.album_open_image);
        outerLayout = findViewById(R.id.songeditmainmenulayout);
        toolbar = findViewById(R.id.songs_edit_toolbar);
        toolbarAlbumName = findViewById(R.id.albumName_on_toolbar);
        progresslayout = findViewById(R.id.songedits_progress_layout);
        gifview = findViewById(R.id.gifimage);

        progressBar = findViewById(R.id.progress_bar_songEdit);
        moreIconFromAlbum = findViewById(R.id.moreIconAlbum);
        et_album_name = findViewById(R.id.et_album_name);
        et_album_artist = findViewById(R.id.et_album_artist);
        et_album_year = findViewById(R.id.et_album_year);
        et_album_genre = findViewById(R.id.et_album_genre);

        til1 = findViewById(R.id.outlinedTextField1);
        til2 = findViewById(R.id.outlinedTextField2);
        til3 = findViewById(R.id.outlinedTextField3);
        til4 = findViewById(R.id.outlinedTextField4);

        final ArrayList arrayList = (ArrayList) getIntent().getSerializableExtra("songs");
        pathList = (ArrayList) getIntent().getSerializableExtra("paths");
        rel_path_list = (ArrayList) getIntent().getSerializableExtra("relpaths");


        String cover = getIntent().getExtras().getString("cover");
        final String album = getIntent().getExtras().getString("album");
        String artist = getIntent().getExtras().getString("artist");


        if (album != null) {
            toolbarAlbumName.setText(album);
        } else {
            toolbarAlbumName.setText("< unknown >");
        }

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    //    toolbarAlbumName.setVisibility(View.INVISIBLE);
                    //   Toast.makeText(Song_Edits.this, "closed", Toast.LENGTH_SHORT).show();

                } else {

                    //    toolbarAlbumName.setText(album);
                    //   Toast.makeText(Song_Edits.this, "opened", Toast.LENGTH_SHORT).show();

                }


            }
        });

        moreIconFromAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playPauseSong();
//                BottomSheetDialog_songs bottomSheetDialog1 = new BottomSheetDialog_songs();
//                bottomSheetDialog1.show(getSupportFragmentManager(), bottomSheetDialog1.getTag());
            }
        });
        open_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                startActivityForResult(i, 7);

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                }

                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        // Code to be executed when an ad finishes loading.
                        mInterstitialAd.show();

                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        // Code to be executed when an ad request fails.
                    }

                    @Override
                    public void onAdOpened() {
                        // Code to be executed when the ad is displayed.
                    }

                    @Override
                    public void onAdClicked() {
                        // Code to be executed when the user clicks on an ad.
                    }

                    @Override
                    public void onAdLeftApplication() {
                        // Code to be executed when the user has left the app.
                    }

                    @Override
                    public void onAdClosed() {
                        // Code to be executed when the interstitial ad is closed.
//                        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                        i.setType("image/*");
//                        startActivityForResult(i, 7);

                    }
                });


            }
        });

        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());

        for (int i = 0; i < arrayList.size(); i++) {
            View v2 = layoutInflater.inflate(R.layout.custom_songs_design, mysonglayout, false);

            TextView name = v2.findViewById(R.id.song_name);
            TextView detail = v2.findViewById(R.id.song_detail_or_singerName);
            ImageView imageView = v2.findViewById(R.id.song_image);
            LinearLayout linearLayout = v2.findViewById(R.id.custom_song_layout);

            final int finalI = i;
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   Toast.makeText(Song_Edits.this, "" + arrayList.get(finalI), Toast.LENGTH_SHORT).show();

                    //      int index = pathList.get(0).toString().lastIndexOf('/');

                    //        moveFile(pathList.get(0).toString().substring(0, index + 1), pathList.get(0).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/");


                }
            });


            name.setText(arrayList.get(i) + "");
            detail.setText(artist);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), getImage(Long.parseLong(cover)));

                imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                try {
                    Uri uri = Uri.parse("android.resource://com.tagmusicapp/drawable/music");

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                    imageView.setImageBitmap(bitmap);
                } catch (Exception e1) {

                }

            }
            mysonglayout.addView(v2);
        }


        album_name.setText(album);
        artist_name.setText(artist);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), getImage(Long.parseLong(cover)));

            appCompatImageView.setImageBitmap(bitmap);
        } catch (Exception e) {

            appCompatImageView.setImageResource(R.drawable.album_icon);

        }

//        Toast.makeText(this, "All songs"+arrayList, Toast.LENGTH_SHORT).show();
//        album_song_list_Adapter albs=new album_song_list_Adapter(getApplicationContext(),arrayList,recyclerView);
//        recyclerView.setAdapter(albs);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));

        fetchdetails(pathList.get(0) + "");

    }


    private Uri getImage(long albumId) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId);
    }

    public void firstView(View v) {
        onBackPressed();
    }

    public void lastView(View v) {

        if (et_album_name.getText().toString().equals("")) {
            til1.setError("Empty field");
            et_album_name.setFocusable(true);
            return;
        }

        if (et_album_artist.getText().toString().equals("")) {
            til2.setError("Empty field");
            et_album_artist.setFocusable(true);
            return;
        }

        if (et_album_year.getText().toString().equals("")) {
            til3.setError("Empty field");
            et_album_year.setFocusable(true);
            return;
        }

        if (et_album_genre.getText().toString().equals("")) {
            til4.setError("Empty field");
            et_album_genre.setFocusable(true);
            return;
        }

        myflag = 0;

        MyHelper myHelper = new MyHelper();
        myHelper.execute();

    }

    public void fetchdetails(String path) {
        File file = new File(path);

        try {
            AudioFile audioFile = AudioFileIO.read(file);
            Tag tag = audioFile.getTagOrCreateAndSetDefault();

            String falbum_title = tag.getFirst(FieldKey.ALBUM);

            String falbum_artist = tag.getFirst(FieldKey.ALBUM_ARTIST);
            //        mArtistEditText.setText(artist);
            //        mArtistEditText.setSelection(artist.length());

            String falbum_year = tag.getFirst(FieldKey.YEAR);
            //    mAlbumEditText.setText(album);
            //  mAlbumEditText.setSelection(album.length());


            String falbum_genre = tag.getFirst(FieldKey.GENRE);
            //   mAlbumArtistEditText.setText(albumArtist);
            // mAlbumArtistEditText.setSelection(albumArtist.length());

            et_album_name.setText(falbum_title);
            et_album_artist.setText(falbum_artist);
            et_album_year.setText(falbum_year);
            et_album_genre.setText(falbum_genre);

//            List<Artwork> artwork = tag.getArtworkList();
//
//            if (artwork.size() > 0) {
//                byte[] image = artwork.get(0).getBinaryData();
//                Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
////                mCardView.setCardBackgroundColor(Palette.from(bitmap).generate().getC(R.color.deep_purple));
//                //       mAlbumArtImage.setImageBitmap(bitmap);
//            }

        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            e.printStackTrace();

        }


//        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};
//        File filest = new File(path);
//        MediaScannerConnection.scanFile(getApplicationContext(), new String[]{filest.getAbsolutePath()}, mimeType /*mimeTypes*/, new MediaScannerConnection.OnScanCompletedListener() {
//            @Override
//            public void onScanCompleted(String s, Uri uri) {
//                Log.e("MyContentUri",""+uri);
//            }
//        });

//
//        MediaMetadataRetriever m = new MediaMetadataRetriever();
//        m.setDataSource(path);
//        // Bitmap thumbnail = m.getFrameAtTime();
//
//        if (Build.VERSION.SDK_INT >= 17) {
//            String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
//            String s2 = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
//            Log.e("Rotation", s+s2);
//        }

//        addMetadata(path, METADATA_KEY_ARTIST, "The Rolling Stones");
//        addMetadata(path, METADATA_KEY_ALBUM, "Sticky Fingers");
//        addMetadata(path, METADATA_KEY_TITLE, "Brown Sugar");
//        retriever.setDataSource(path);
//        assertThat(retriever.extractMetadata(METADATA_KEY_ARTIST)).isEqualTo("The Rolling Stones");
//        assertThat(retriever.extractMetadata(METADATA_KEY_ALBUM)).isEqualTo("Sticky Fingers");
//        assertThat(retriever.extractMetadata(METADATA_KEY_TITLE)).isEqualTo("Brown Sugar");


        //       Uri uri= Uri.fromFile(new File(path));

//        ContentValues content = new ContentValues();
//
//        content.put(MediaStore.Audio.Media.ARTIST,
//                "Vijay r");
//
//
//
//        ContentResolver resolver = context.getContentResolver();
//
//        Uri uri= ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,36221);
//
//        resolver.update(uri,content, null,null);

        //   sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));


    }

    void setDetails(int type) {

        for (int i = 0; i < pathList.size(); i++) {

            int index = pathList.get(i).toString().lastIndexOf('/');

            ArrayList rootDir = getRemovableStorageRoots(getApplicationContext());


            if (!rootDir.isEmpty() && pathList.get(i).toString().contains(rootDir.get(0).toString())) {

                sdcardStorage = rootDir.get(0).toString();
                moveFile(i, pathList.get(i).toString().substring(0, index + 1), pathList.get(i).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/", type);
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathList.get(i) + ""))));


            } else {

                //        moveFile(pathList.get(i).toString().substring(0, index + 1), pathList.get(i).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/");


                //   Uri uri= Uri.parse("file:///"+ pathList.get(0));

                File file = new File(pathList.get(i) + "");

//            File file = new File(uri+"");

                if (type == 0) {
                    try {
                        AudioFile audioFile = AudioFileIO.read(file);

                        Tag tag = audioFile.getTagOrCreateAndSetDefault();

                        tag.setField(FieldKey.ALBUM, et_album_name.getText() + "");

                        tag.setField(FieldKey.ALBUM_ARTIST, et_album_artist.getText() + "");

                        tag.setField(FieldKey.YEAR, et_album_year.getText() + "");

                        tag.setField(FieldKey.GENRE, et_album_genre.getText() + "");

                        audioFile.setTag(tag);

                        audioFile.commit();

                        try {
                            AudioFileIO.write(audioFile);
                        } catch (CannotWriteException e) {

                        }


                    } catch (Exception e) {

                    }
                } else {


                    Bitmap tmbitmap = myLoadedBitmap;

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    // myLoadedBitmap.recycle();


//
//                    Uri imageUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id));
//                    Bitmap scaledbmp = decodeSampledBitmapFromResource(imageUri, 300, 300);
//                    if (scaledbmp != null) {
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        scaledbmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] byteArray = stream.toByteArray();
//

                    setMp3AlbumArt(file, byteArray);
                }

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathList.get(i) + ""))));
            }

        }
    }


    private void moveFile(int myRelIndex, String inputPath, String inputFile, String outputPath, int type) {

        InputStream in = null;
        OutputStream out = null;

        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);

            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();

            File file = new File(outputPath + "/" + inputFile);

            try {

                if (type == 0) {

                    AudioFile audioFile = AudioFileIO.read(file);

                    Tag tag = audioFile.getTagOrCreateAndSetDefault();

                    tag.setField(FieldKey.ALBUM, et_album_name.getText() + "");

                    tag.setField(FieldKey.ALBUM_ARTIST, et_album_artist.getText() + "");

                    tag.setField(FieldKey.YEAR, et_album_year.getText() + "");

                    tag.setField(FieldKey.GENRE, et_album_genre.getText() + "");

                    audioFile.setTag(tag);

                    audioFile.commit();
                    try {
                        AudioFileIO.write(audioFile);

                    } catch (CannotWriteException e) {

                    }
                } else {
                    Bitmap tmbitmap = myLoadedBitmap;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    // myLoadedBitmap.recycle();
                    setMp3AlbumArt(file, byteArray);
                }


                //  String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

                SharedPreferences sharedPreferences = getSharedPreferences("saf", MODE_PRIVATE);
                String mySafUri = sharedPreferences.getString("saf", "");

//                ArrayList rootS=getRemovableStorageRoots(getApplicationContext());
//
//                StringBuilder sb=new StringBuilder("");
//
//                String comtext=inputPath+inputFile;
//                int comindex=comtext.charAt('/');
//
//                String myFinalPath=(inputPath).substring(rootS.get(0).toString().length()+1);
//
//                for(int i=0;i<myFinalPath.length()-1;i++)
//                {
//                    if(myFinalPath.charAt(i)=='/')
//                    {
//                        sb.append("%2F");
//                    }
//                    else if(myFinalPath.charAt(i)==' ')
//                    {
//                        sb.append("%20");
//                    }
//                    else if(myFinalPath.charAt(i)=='+')
//                    {
//                        sb.append("%2B");
//                    }
//                    else if(myFinalPath.charAt(i)=='*')
//                    {
//                        sb.append("%C3%B7");
//                    }
//                    else if(myFinalPath.charAt(i)=='=')
//                    {
//                        sb.append("%3D");
//                    }
//                    else if(myFinalPath.charAt(i)=='%')
//                    {
//                        sb.append("%25");
//                    }
//                    else if(myFinalPath.charAt(i)=='&')
//                    {
//                        sb.append("%26");
//                    }
//                    else if(myFinalPath.charAt(i)=='@')
//                    {
//                        sb.append("%40");
//                    }
//                    else if(myFinalPath.charAt(i)=='$')
//                    {
//                        sb.append("%24");
//                    }
//                    else if(myFinalPath.charAt(i)=='#')
//                    {
//                        sb.append("%23");
//                    }
//                    else if(myFinalPath.charAt(i)=='₹')
//                    {
//                        sb.append("%E2%82%B9");
//                    }
//                    else
//                    {
//                        sb.append(myFinalPath.charAt(i));
//                    }
//                }
//
//                String pathg=mySafUri+sb.toString();
//
//                Log.e("MyFinalPath",myFinalPath);
//                Log.e("Roots",rootS+"");
//                Log.e("MySaf",mySafUri+"");
//                Log.e("My Sb",sb+"");
//                Log.e("My inputfilee",inputFile+"");
//                Log.e("MyConvSAF",mySafUri+sb.toString());

                //      grantUriPermission(getPackageName(), Uri.parse(mySafUri), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );

//                grantUriPermission(getPackageName(), Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                final int takeFlags =   (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                getContentResolver().takePersistableUriPermission(Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), takeFlags);

//                        DocumentFile pickedDirr = DocumentFile.fromTreeUri(getApplicationContext(), Uri.parse("/3863-6134/audio/media/70052"));
//content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
                // DocumentFile pickedDir = pickedDirr.createFile(mimeType[0], inputFile);

                DocumentFile pickedDirr = findMySafDir(inputPath.substring(sdcardStorage.length()) + "", Uri.parse(mySafUri), inputFile);

                moveFileBackBySaf(outputPath, inputFile, pickedDirr);


                //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
                //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit

//                SafHelper safHelper=new SafHelper(getApplicationContext(),"/storage/3863-6134/",1);
//                safHelper.requestPermissions(Song_Edits.this);

            } catch (Exception e) {

            }


        } catch (FileNotFoundException fnfe1) {


        } catch (Exception e) {


        }

        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathList.get(myRelIndex) + ""))));

    }


    private void moveFileBackBySaf(String inputPath, String inputFile, DocumentFile sdDir) {

        //   Log.e("inputfile", "" + completePath);
        //     Log.e("outputpath",""+outputPath);

        InputStream in = null;
        OutputStream out = null;
        try {

            String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3");


//            //create output directory if it doesn't exist
//            File dir = new File (outputPath);
//            if (!sdDir.exists())
//            {
//                sdDir.getParentFile().createFile(mimeType,"newfile.mp3");
//            }


            in = new FileInputStream(inputPath + inputFile);


            out = getApplicationContext().getContentResolver().openOutputStream(sdDir.getUri());

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);

            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();


        } catch (FileNotFoundException fnfe1) {

        } catch (Exception e) {
        }

    }

    //    public static boolean copyFileToTargetSAFFolder(Context context, String filePath, String targetFolder, String destFileName )
//    {
//
//
//        Uri uri = Uri.parse(targetFolder);
//
//        String docId = DocumentsContract.getDocumentId(uri);
//        Uri dirUri = DocumentsContract.buildDocumentUriUsingTree(uri, docId );
//
//        Uri destUri = null;
//
//        try
//        {
//            destUri = DocumentsContract.createDocument(context.getContentResolver(), dirUri, "*/*", destFileName);
//
//        } catch (FileNotFoundException e )
//        {
//            e.printStackTrace();
//
//            return false;
//        }
//
//        InputStream is = null;
//        OutputStream os = null;
//        try {
//            is = new FileInputStream(filePath);
//
//            os = context.getContentResolver().openOutputStream( destUri, "w");
//
//            byte[] buffer = new byte[1024];
//
//            int length;
//            while ((length = is.read(buffer)) > 0)
//                os.write(buffer, 0, length);
//                Log.e("myBuf",buffer+"");
//            is.close();
//            os.flush();
//            os.close();
//
//            return true;
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e)     {
//            e.printStackTrace();
//        }
//
//        return false;
//    }
//

    public static ArrayList getRemovableStorageRoots(Context context) {
        File[] roots = context.getExternalFilesDirs("external");
        ArrayList<File> rootsArrayList = new ArrayList<>();
        for (int i = 0; i < roots.length; i++) {
            if (roots[i] != null) {
                String path = roots[i].getPath();
                int index = path.lastIndexOf("/Android/data/");
                if (index > 0) {
                    path = path.substring(0, index);
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath())) {
                        rootsArrayList.add(new File(path));
                    }
                }
            }
        }

        Log.e("MyPaths", rootsArrayList + "");
//
//        roots = new File[rootsArrayList.size()];
//        rootsArrayList.toArray(roots);
        return rootsArrayList;
    }


    private void moveFileBackToPrimary(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();

        } catch (Exception e) {

        }

    }

    DocumentFile findMySafDir(String relative_path, Uri uri, String filename) {

        ArrayList pathFragments = new ArrayList();

        StringBuilder sb = new StringBuilder("");

        for (int i = 0; i < relative_path.length(); i++) {
            if (relative_path.charAt(i) == '/') {
                pathFragments.add(sb + "");
                sb = new StringBuilder("");
            } else {
                sb.append(relative_path.charAt(i));
            }
        }

        DocumentFile documentFile = DocumentFile.fromTreeUri(getApplicationContext(), uri);
        //  DocumentFile[] docs=documentFile.listFiles();
        DocumentFile[] docs;
        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

        String pathUnit = "";
        for (int j = 0; j < pathFragments.size(); j++) {
            pathUnit = pathFragments.get(j) + "";

            docs = documentFile.listFiles();

            for (int i = 0; i < docs.length; i++) {
                if (docs[i].getName().toString().equals(pathUnit)) {
                    documentFile = docs[i];

                    break;
                } else {

                }
            }

        }


        DocumentFile ret_doc_fi = documentFile.findFile(filename);

        ret_doc_fi.delete();

        DocumentFile ret_doc_file = documentFile.createFile(mimeType[0], filename);

        return ret_doc_file;
    }

    //    public Uri getUri() {
//        List<UriPermission> persistedUriPermissions = getContentResolver().getPersistedUriPermissions();
//        if (persistedUriPermissions.size() > 0) {
//            UriPermission uriPermission = persistedUriPermissions.get(0);
//            Log.e("Permg",uriPermission+"");
//            return uriPermission.getUri();
//        }
//        return null;
//    }
//
    private void scanMedia(String path) {
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        Intent scanFileIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        sendBroadcast(scanFileIntent);
    }

//    @Override
//    public void SongsBottomSheetClicked(int id)
//    {
//        switch (id) {
//            case R.id.remove_tag1:
//                Toast.makeText(this, "remove tag", Toast.LENGTH_SHORT).show();
//                break;
//
//            case R.id.remove_artwork1:
//                Toast.makeText(this, "remove artwork", Toast.LENGTH_SHORT).show();
//
//                break;
//
//            case R.id.add_cover_from_gallery1:
//                Toast.makeText(this, "add cover from gallery", Toast.LENGTH_SHORT).show();
//                Intent i=new Intent(Intent.ACTION_GET_CONTENT);
//                i.setType("image/*");
//                startActivityForResult(i,7);
//
//                break;
//
////            case R.id.paste_url1:
////                Toast.makeText(this, "paste url", Toast.LENGTH_SHORT).show();
////                break;
////
////            case R.id.search_web1:
////                Toast.makeText(this, "serch on web", Toast.LENGTH_SHORT).show();
////                break;
//
//            case R.id.save_memory1:
//
//                Toast.makeText(this, "save cover to memory", Toast.LENGTH_SHORT).show();
//
//                break;
//
////            case R.id.delete_file1:
////                Toast.makeText(this, "delete file", Toast.LENGTH_SHORT).show();
////                break;
//
//            case R.id.play1:
//
//                try {
//
//
//
//                    if (flag==0) {
//
//                        mediaPlayer = new MediaPlayer();
//                        mediaPlayer.setDataSource(pathList.get(0).toString());
//                        mediaPlayer.prepare();
//                        mediaPlayer.start();
//                        flag=1;
//
//                    }
//                    else if(flag==1)
//                    {
//
//
//                        mediaPlayer.stop();
//
//                        flag=0;
//
//                        Toast.makeText(this, "Stop song", Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } catch (Exception e) {
//                    Toast.makeText(this, "exception" + e.toString(), Toast.LENGTH_SHORT).show();
//                }
//                break;
//
//        }
//    }

    class MyHelper extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            setDetails(myflag);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresslayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Glide.with(getApplicationContext()).load(R.drawable.check).into(gifview);

            progressBar.setVisibility(View.INVISIBLE);
            gifview.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {


                    try {
                        Thread.sleep(2500);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progresslayout.setVisibility(View.GONE);
                                onBackPressed();
                            }
                        });


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();


            //   onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra("data", "album");
        startActivity(i);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 7) {
            if (data == null)
                return;
            Uri uri = data.getData();
            try {
                myLoadedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                myflag = 1;
                MyHelper myHelper = new MyHelper();
                myHelper.execute();
            } catch (Exception e) {

            }
        }

    }

    public String setMp3AlbumArt(File SourceFile, byte[] bytes) {
        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg")};
        String error = null;
        try {
            MP3File musicFile = (MP3File) AudioFileIO.read(SourceFile);
            AbstractID3v2Tag tag = musicFile.getID3v2Tag();
            String mimeTyp[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("png")};
            if (tag == null) {

            }

            if (tag != null) {
                Artwork artwork = null;
                try {
                    artwork = ArtworkFactory.createArtworkFromFile(SourceFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    error = e.getMessage();
                }
                if (artwork != null) {

                    artwork.setBinaryData(bytes);
                    artwork.setMimeType(mimeTyp[0]);
                    tag.deleteArtworkField();
                    tag.setField(artwork);

                    musicFile.setTag(tag);
                    musicFile.commit();
                } else {

                    artwork.setBinaryData(bytes);
                    tag.addField(artwork);
                    tag.setField(artwork);
                    musicFile.setTag(tag);
                    musicFile.commit();
                }
            }

        } catch (Exception e) {
            error = e.getMessage();

        }
        return error;
    }

    public void playPauseSong() {
        try {


            if (flag == 0) {

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(pathList.get(0).toString());
                mediaPlayer.prepare();
                mediaPlayer.start();
                flag = 1;
                moreIconFromAlbum.setBackgroundResource(R.drawable.player_pause);

            } else if (flag == 1) {


                mediaPlayer.stop();
                moreIconFromAlbum.setBackgroundResource(R.drawable.player_playt);
                flag = 0;


            }


        } catch (Exception e) {

        }
    }

}
