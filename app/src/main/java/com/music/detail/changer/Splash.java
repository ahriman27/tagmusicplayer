package com.music.detail.changer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

import java.util.Objects;

public class Splash extends AppCompatActivity {
VideoView splashVideo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //     getSupportActionBar().hide();

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        splashVideo= findViewById(R.id.splashVideo);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.splash_video);
        splashVideo.setVideoURI(uri);
        splashVideo.start();
        final SharedPreferences sharedPreferences = getSharedPreferences("perm", MODE_PRIVATE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    Thread.sleep(3000);

                    if (Objects.equals(sharedPreferences.getString("perm", "none"), "none")) {
                        Intent i = new Intent(getApplicationContext(), Permissions.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("data", "so");
                        startActivity(i);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}