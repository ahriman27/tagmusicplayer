package com.music.detail.changer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetDialog_Albums extends BottomSheetDialogFragment {
    BottomSheetListener bottomSheetDialog;
    LinearLayout removeTag, removeArtwork, addCover, PasteCover, SearchWeb, SaveCover, deleteFile, Play;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_layout, container, false);

        removeTag = view.findViewById(R.id.remove_tag);
        removeArtwork = view.findViewById(R.id.remove_artwork);
        addCover = view.findViewById(R.id.add_cover_from_gallery);
//        PasteCover=view.findViewById(R.id.paste_url);
//        SearchWeb=view.findViewById(R.id.search_web);
        SaveCover = view.findViewById(R.id.save_memory);
        //deleteFile=view.findViewById(R.id.delete_file);
        Play = view.findViewById(R.id.play);


        removeTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.onBottomSheetClicked(R.id.remove_tag);

                dismiss();
            }
        });


        removeArtwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.onBottomSheetClicked(R.id.remove_artwork);

                dismiss();
            }
        });


        addCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.onBottomSheetClicked(R.id.add_cover_from_gallery);

                dismiss();
            }
        });


//        PasteCover.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bottomSheetDialog.onBottomSheetClicked(R.id.paste_url);
//
//                dismiss();
//            }
//        });
//
//
//
//        SearchWeb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bottomSheetDialog.onBottomSheetClicked(R.id.search_web);
//
//                dismiss();
//            }
//        });


        SaveCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.onBottomSheetClicked(R.id.save_memory);

                dismiss();
            }
        });


//
//        deleteFile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bottomSheetDialog.onBottomSheetClicked(R.id.delete_file);
//
//                dismiss();
//            }
//        });

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.onBottomSheetClicked(R.id.play);

                dismiss();
            }
        });

        return view;
    }

    public interface BottomSheetListener {
        void onBottomSheetClicked(int id);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof BottomSheetListener) {
            bottomSheetDialog = (BottomSheetListener) context;
        } else {
            // Error Code
        }
    }
}
