package com.music.detail.changer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.roacult.backdrop.BackdropLayout;
import com.music.detail.changer.Album_package.Albums_Fragment;
import com.music.detail.changer.Fragments.More_fragment;
import com.music.detail.changer.Fragments.SongsFragment;
import com.music.detail.changer.Fragments.FileFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    BackdropLayout backdropLayout;
    Fragment fragment;
    Fragment songsFrag, albumFrag, fileFlag;
    int MenuIds;
    RelativeLayout relativeLayout;
    public int flag = 1;
    int fragid = 0;
    BottomNavigationView navigation;
    private InterstitialAd mInterstitialAd;
    AdView adView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new SongsFragment());
        mInterstitialAd = new InterstitialAd(MainActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8305515768751170/6479165367");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {

        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {


            }
        });


        // backdropLayout= findViewById(R.id.back_drop_layout);

        navigation = findViewById(R.id.bottomNavigationView);
        navigation.setOnNavigationItemSelectedListener(this);
        relativeLayout = findViewById(R.id.mainrelativelayout);
        //    getSupportActionBar().hide();


        final SharedPreferences sharedPreferences = getSharedPreferences("touch", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.getString("touch", "").equals("")) {
            relativeLayout.setVisibility(View.VISIBLE);
        }

        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (sharedPreferences.getString("touch", "").equals("")) {
                    editor.putString("touch", "touch");
                    editor.commit();
                    relativeLayout.setVisibility(View.GONE);
                    return true;
                } else {
                    return false;
                }
            }
        });


        String data = getIntent().getExtras().getString("data");

        if (data != null) {
            switch (data) {
                case "song": {
                    refreshSongsFragment();
                    break;
                }
                case "album": {
                    refreshAlbumsFragment();
                    break;
                }
                case "file": {
                    refreshFileFragment();
                    break;
                }
            }
        }
        songsFrag = new SongsFragment();
        albumFrag = new Albums_Fragment();
        fileFlag = new FileFragment();

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        fragment = null;

        switch (item.getItemId()) {
            case R.id.bottomNavigationSongsMenuId:
                MenuIds = R.id.bottomNavigationSongsMenuId;
                fragment = new SongsFragment();
                break;

            case R.id.bottomNavigationAlbumsMenuId:
                MenuIds = R.id.bottomNavigationAlbumsMenuId;
                fragment = new Albums_Fragment();
                break;

            case R.id.bottomNavigationFilesMenuId:
                MenuIds = R.id.bottomNavigationFilesMenuId;
                fragment = new FileFragment();
                break;
            case R.id.bottomNavigationMoreMenuId:
                MenuIds = R.id.bottomNavigationMoreMenuId;
                fragment = new More_fragment();
                break;
        }
        return loadFragment(fragment);
    }


    @Override
    public void onBackPressed() {
//        switch (MenuIds)
//        {
//            case R.id.bottomNavigationSongsMenuId:
//                Toast.makeText(this, "Songs menu backed", Toast.LENGTH_SHORT).show();
//            break;
//
//            case R.id.bottomNavigationAlbumsMenuId:
//                break;
//
//            case R.id.bottomNavigationFilesMenuId:
//                Toast.makeText(this, "Files menu backed", Toast.LENGTH_SHORT).show();
//                break;
//
//            case R.id.bottomNavigationMoreMenuId:
//                Toast.makeText(this, "More menu backed", Toast.LENGTH_SHORT).show();
//                break;
//
//        }

        if (flag == 0) {
            refreshSongsFragment();
            flag = 1;
        } else {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Are you Sure you want to exit ? ")
                        .setCancelable(false)
                        .setIcon(R.drawable.vector_exit)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();

                alert.show();
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            } catch (Exception e) {
                //  Toast.makeText(context,e+"",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void refreshFileFragment() {
        navigation.setSelectedItemId(R.id.bottomNavigationFilesMenuId);
    }

    public void refreshSongsFragment() {
        navigation.setSelectedItemId(R.id.bottomNavigationSongsMenuId);
    }

    public void refreshAlbumsFragment() {
        navigation.setSelectedItemId(R.id.bottomNavigationAlbumsMenuId);
    }

}