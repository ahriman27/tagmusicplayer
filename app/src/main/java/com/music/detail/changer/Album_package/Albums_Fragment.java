package com.music.detail.changer.Album_package;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SearchView;
import android.widget.Spinner;

import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.roacult.backdrop.BackdropLayout;
import com.music.detail.changer.Fragments.SongAdapters.SongLoader;
import com.music.detail.changer.MainActivity;
import com.music.detail.changer.R;

import java.util.ArrayList;
import java.util.Collections;


public class Albums_Fragment extends Fragment implements SearchView.OnQueryTextListener {
Spinner spinner,namespinner;
ArrayList a,b,c,d,e,f;
int order_flag=0,sort_flag=0;
Toolbar toolbar_a;
SearchView album_searchView;

BackdropLayout backdropLayout;
Albums_Adapter albums_adapter;
ArrayList<String> totalAlbums;
AdView adView;
    ArrayList nested_songs;
    ArrayList album_cover;
    ArrayList artist_list;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adView =view.findViewById(R.id.albumsFragmentAdview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
         GridView gridView=view.findViewById(R.id.all_albums);
        LinearLayout linearLayout=view.findViewById(R.id.album_progress_layout);
        toolbar_a=view.findViewById(R.id.toolbar_albums);
        backdropLayout=view.findViewById(R.id.back_drop_layout);
       linearLayout.setVisibility(View.GONE);
        album_searchView=view.findViewById(R.id.album_search_view);


        String [] aa={"Ascending","Descending"};
        String [] bb={"Album","Artist"};

        spinner=view.findViewById(R.id.song_sort_spinner);
        namespinner=view.findViewById(R.id.song_order_spinner);





        toolbar_a.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.toolbar_search_id:
                    {
                        album_searchView.setVisibility(View.VISIBLE);
                        album_searchView.setOnQueryTextListener(Albums_Fragment.this);
                        return true;
                    }

                    case R.id.toobar_refresh_id: {

//                        albums_adapter.notifyDataSetChanged();
                        ((MainActivity)getActivity()).refreshAlbumsFragment();
                        return true;
                    }

                    case R.id.toolbar_sort_id: {

                        backdropLayout.open();

                        return true;
                    }

                }
                return true;
            }
        });

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy,bb);
        spinner.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy,aa);
        namespinner.setAdapter(adapter2);

        final SongLoader sg=new SongLoader();

        ArrayList arrayListSongs=sg.getSongsYAlbum(getActivity());
        ArrayList arrayListAlbum =sg.getdetailYAlbum(getActivity());
        ArrayList album_cover_all=sg.getAlbumYAlbum(getActivity());
        ArrayList artist_cover_all=sg.getArtistYAlbum(getActivity());
        ArrayList myPath=sg.getPathYAlbum(getActivity());
        ArrayList myRelPath=sg.getMyRelPathByAlbum(getActivity());


       // Log.i("song"+arrayListSongs.size(),arrayListSongs+"");
        //Log.i("album"+arrayListAlbum.size(),arrayListAlbum+"");


        fetchMyData(arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath);

                albums_adapter=new Albums_Adapter(getActivity(),a,b,c,d,e,f);
                gridView.setAdapter(albums_adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0)
                {
                    sort_flag=0;

                    ArrayList arrayListSongs=sg.getSongsYAlbum(getActivity());
                    ArrayList arrayListAlbum =sg.getdetailYAlbum(getActivity());
                    ArrayList album_cover_all=sg.getAlbumYAlbum(getActivity());
                    ArrayList artist_cover_all=sg.getArtistYAlbum(getActivity());
                    ArrayList myPath=sg.getPathYAlbum(getActivity());
                    ArrayList myRelPath=sg.getMyRelPathByAlbum(getActivity());


             //       Log.i("song"+arrayListSongs.size(),arrayListSongs+"");
             //       Log.i("album"+arrayListAlbum.size(),arrayListAlbum+"");

                    fetchMyData(arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath);

                    if(order_flag==1)
                    {
                        Collections.reverse(a);
                        Collections.reverse(b);
                        Collections.reverse(c);
                        Collections.reverse(d);
                        Collections.reverse(e);
                        Collections.reverse(f);
                    }

                    albums_adapter.albums_List=a;
                    albums_adapter.nested_Songs=b;
                    albums_adapter.cover_List=c;
                    albums_adapter.artistList=d;
                    albums_adapter.path_List=e;
                    albums_adapter.path_Rel_List=f;

                    albums_adapter.notifyDataSetChanged();

                }
                else
                {
                    sort_flag=1;

                    ArrayList arrayListSongs=sg.getSongsYArtist(getActivity());
                    ArrayList arrayListAlbum =sg.getdetailYArtist(getActivity());
                    ArrayList album_cover_all=sg.getAlbumYArtist(getActivity());
                    ArrayList artist_cover_all=sg.getArtistYArtist(getActivity());
                    ArrayList myPath=sg.getPathYArtist(getActivity());
                    ArrayList myRelPath=sg.getMyRelPathByArtist(getActivity());

             //       Log.i("song"+arrayListSongs.size(),arrayListSongs+"");
              //      Log.i("album"+arrayListAlbum.size(),arrayListAlbum+"");

                    fetchMyData(arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath);

                    if(order_flag==1)
                    {
                        Collections.reverse(a);
                        Collections.reverse(b);
                        Collections.reverse(c);
                        Collections.reverse(d);
                        Collections.reverse(e);
                        Collections.reverse(f);
                    }

                    albums_adapter.albums_List=a;
                    albums_adapter.nested_Songs=b;
                    albums_adapter.cover_List=c;
                    albums_adapter.artistList=d;
                    albums_adapter.path_List=e;
                    albums_adapter.path_Rel_List=f;

                    albums_adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        namespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0)
                {
                    order_flag=0;

                    ArrayList arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath;

                    if(sort_flag==0) {
                         arrayListSongs = sg.getSongsYAlbum(getActivity());
                         arrayListAlbum = sg.getdetailYAlbum(getActivity());
                         album_cover_all = sg.getAlbumYAlbum(getActivity());
                         artist_cover_all = sg.getArtistYAlbum(getActivity());
                         myPath=sg.getPathYAlbum(getActivity());
                        myRelPath=sg.getMyRelPathByAlbum(getActivity());
                    }else
                    {
                         arrayListSongs=sg.getSongsYArtist(getActivity());
                         arrayListAlbum =sg.getdetailYArtist(getActivity());
                         album_cover_all=sg.getAlbumYArtist(getActivity());
                         artist_cover_all=sg.getArtistYArtist(getActivity());
                        myPath=sg.getPathYArtist(getActivity());
                        myRelPath=sg.getMyRelPathByArtist(getActivity());
                    }

               //     Log.i("song"+arrayListSongs.size(),arrayListSongs+"");
               //     Log.i("album"+arrayListAlbum.size(),arrayListAlbum+"");

                    fetchMyData(arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath);



                    albums_adapter.albums_List=a;
                    albums_adapter.nested_Songs=b;
                    albums_adapter.cover_List=c;
                    albums_adapter.artistList=d;
                    albums_adapter.path_List=e;
                    albums_adapter.path_Rel_List=f;

                    albums_adapter.notifyDataSetChanged();

                }
                else
                {

                    order_flag=1;

                    ArrayList arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath;

                    if(sort_flag==0) {
                        arrayListSongs = sg.getSongsYAlbum(getActivity());
                        arrayListAlbum = sg.getdetailYAlbum(getActivity());
                        album_cover_all = sg.getAlbumYAlbum(getActivity());
                        artist_cover_all = sg.getArtistYAlbum(getActivity());
                        myPath=sg.getPathYAlbum(getActivity());
                        myRelPath=sg.getMyRelPathByAlbum(getActivity());
                    }else
                    {
                        arrayListSongs=sg.getSongsYArtist(getActivity());
                        arrayListAlbum =sg.getdetailYArtist(getActivity());
                        album_cover_all=sg.getAlbumYArtist(getActivity());
                        artist_cover_all=sg.getArtistYArtist(getActivity());
                        myPath=sg.getPathYArtist(getActivity());
                        myRelPath=sg.getMyRelPathByArtist(getActivity());
                    }

              //      Log.i("song"+arrayListSongs.size(),arrayListSongs+"");
               //     Log.i("album"+arrayListAlbum.size(),arrayListAlbum+"");

                    fetchMyData(arrayListSongs,arrayListAlbum,album_cover_all,artist_cover_all,myPath,myRelPath);


                        Collections.reverse(a);
                        Collections.reverse(b);
                        Collections.reverse(c);
                        Collections.reverse(d);
                    Collections.reverse(e);


                    albums_adapter.albums_List=a;
                    albums_adapter.nested_Songs=b;
                    albums_adapter.cover_List=c;
                    albums_adapter.artistList=d;
                    albums_adapter.path_List=e;
                    albums_adapter.path_Rel_List=f;

                    albums_adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        return view;
    }


//    public class MyBackWork extends AsyncTask<Void,Void,Void>
//    {
//        GridView gridView;
//        LinearLayout linearLayout;
//        MyBackWork(GridView gridView, LinearLayout linearLayout)
//        {
//            this.gridView=gridView;
//            this.linearLayout=linearLayout;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//
//            linearLayout.setVisibility(View.GONE);
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//
//
//            return null;
//        }
//    }



    void fetchMyData(ArrayList arrayListSongs,ArrayList arrayListAlbum,ArrayList album_cover_all,ArrayList artist_cover_all,ArrayList myPath,ArrayList myRelPath)
    {

         totalAlbums=new ArrayList();
         album_cover=new ArrayList();
         artist_list=new ArrayList();

        for(int i=0;i<arrayListAlbum.size();i++)
        {
            if(!totalAlbums.contains(arrayListAlbum.get(i)+""))
            {
                totalAlbums.add(arrayListAlbum.get(i)+"");
                try {
                    album_cover.add(album_cover_all.get(i).toString());

                }
                catch (Exception e)
                {
                    album_cover.add(null);

                }
                artist_list.add(artist_cover_all.get(i)+"");
            }
        }



        nested_songs=new ArrayList();
        ArrayList inner_songs=new ArrayList();

        ArrayList nested_path=new ArrayList();
        ArrayList inner_path=new ArrayList();

        ArrayList nested_Rel_path=new ArrayList();
        ArrayList inner_Rel_path=new ArrayList();


        for(int i=0;i<totalAlbums.size();i++)
        {

            inner_songs=new ArrayList();
            inner_path=new ArrayList();
            inner_Rel_path=new ArrayList();

            while(arrayListAlbum.indexOf(totalAlbums.get(i))!=-1)
            {
                inner_songs.add(arrayListSongs.get(arrayListAlbum.indexOf(totalAlbums.get(i))));
                inner_path.add(myPath.get(arrayListAlbum.indexOf(totalAlbums.get(i))));
                inner_Rel_path.add(myRelPath.get(arrayListAlbum.indexOf(totalAlbums.get(i))));
                arrayListAlbum.set(arrayListAlbum.indexOf(totalAlbums.get(i)),"\tAlbum-Done\t");
            }

            nested_songs.add(inner_songs);
            nested_path.add(inner_path);
            nested_Rel_path.add(inner_Rel_path);
        }

      //  Log.i("totalalbum"+totalAlbums.size(),totalAlbums+"");
     //   Log.i("nestedsongs"+nested_songs.size(),nested_songs+"");
      //  Log.i("albumcover"+album_cover.size(),album_cover+"");
      //  Log.i("nestedpath"+nested_path.size(),nested_path+"");

        a=totalAlbums;
        b=nested_songs;
        c=album_cover;
        d=artist_list;
        e=nested_path;
        f=nested_Rel_path;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText == null || newText.trim().isEmpty()) {
            albums_adapter.setFilter(a,b,c,d,e,f);
            return false;
        }

        newText = newText.toLowerCase();
        final ArrayList filteredalbumList = new ArrayList<>();
        final ArrayList filteredartistList = new ArrayList<>();
        final ArrayList filteredcoverList = new ArrayList<>();
        final ArrayList filteredsongsList = new ArrayList<>();
        final ArrayList filteredpathList = new ArrayList<>();
        final ArrayList filteredRelpathList = new ArrayList<>();

        for (int i=0;i<totalAlbums.size();i++)
        {
            final String title = a.get(i).toString().toLowerCase();
            final String artist = b.get(i).toString().toLowerCase();

            if ((title.contains(newText)) || (artist.contains(newText)))
            {
                filteredalbumList.add(a.get(i));
                filteredsongsList.add(b.get(i));
                filteredcoverList.add(c.get(i));
                filteredartistList.add(d.get(i));
                filteredpathList.add(e.get(i));
                filteredRelpathList.add(f.get(i));
            }
        }
        albums_adapter.setFilter(filteredalbumList,filteredartistList,filteredcoverList,filteredsongsList,filteredpathList,filteredRelpathList);
        return true;
    }


}