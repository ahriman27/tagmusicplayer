package com.music.detail.changer.Fragments.SongAdapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.music.detail.changer.R;

public class BottomSheetDialog_songs extends BottomSheetDialogFragment {

    SongsBottomSheetListener songsBottomSheetListener;
    LinearLayout removeTag, removeArtwork, addCover, PasteCover, SearchWeb, SaveCover, deleteFile, Play;
    TextView playSongText;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_layout_1, container, false);

        removeTag = view.findViewById(R.id.remove_tag1);
        removeArtwork = view.findViewById(R.id.remove_artwork1);
        addCover = view.findViewById(R.id.add_cover_from_gallery1);
//        PasteCover = view.findViewById(R.id.paste_url1);
//        SearchWeb = view.findViewById(R.id.search_web1);
        SaveCover = view.findViewById(R.id.save_memory1);
//        deleteFile = view.findViewById(R.id.delete_file1);
        playSongText= view.findViewById(R.id.playSong);
        Play = view.findViewById(R.id.play1);

        view.setBackgroundColor(Color.TRANSPARENT);

        removeTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                songsBottomSheetListener.SongsBottomSheetClicked(R.id.remove_tag1);
                dismiss();
            }
        });


        removeArtwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                songsBottomSheetListener.SongsBottomSheetClicked(R.id.remove_artwork1);
                dismiss();
            }
        });


        addCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                songsBottomSheetListener.SongsBottomSheetClicked(R.id.add_cover_from_gallery1);
                dismiss();
            }
        });


//        PasteCover.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                songsBottomSheetListener.SongsBottomSheetClicked(R.id.paste_url1);
//                dismiss();
//            }
//        });


//        SearchWeb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                songsBottomSheetListener.SongsBottomSheetClicked(R.id.search_web1);
//                dismiss();
//            }
//        });
//

        SaveCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                songsBottomSheetListener.SongsBottomSheetClicked(R.id.save_memory1);
                dismiss();
            }
        });

//
//        deleteFile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                songsBottomSheetListener.SongsBottomSheetClicked(R.id.delete_file1);
//                dismiss();
//            }
//        });

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                songsBottomSheetListener.SongsBottomSheetClicked(R.id.play1);
              dismiss();
            }
        });

        return view;
    }


    public interface SongsBottomSheetListener {
        void SongsBottomSheetClicked(int id);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof SongsBottomSheetListener) {
            songsBottomSheetListener = (SongsBottomSheetListener) context;
        } else {
            // Error Code
        }

    }

    @Override
    public int getTheme() {

            return R.style.AppBottomSheetDialogTheme;

    }
}
