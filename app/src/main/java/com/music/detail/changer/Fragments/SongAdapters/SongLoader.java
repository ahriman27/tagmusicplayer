package com.music.detail.changer.Fragments.SongAdapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

public class SongLoader {
    public List<SongModel> getAllSongs(Context context) {
        List<SongModel> songList = new ArrayList<>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };
        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                songList.add(new SongModel(cursor.getLong(0), cursor.getString(1), cursor.getLong(2),
                        cursor.getString(3), cursor.getLong(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7)));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return songList;


    }

    public ArrayList getSongsYAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };


        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(1));

            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getdetailYAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(3));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getAlbumYAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(2));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getArtistYAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);


        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(5));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getPathYAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                MediaStore.Audio.AudioColumns.DATA,//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };


        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(0));

            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getPathYArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                MediaStore.Audio.AudioColumns.DATA,//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };


        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(0));

            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getSongsYArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };


        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(1));

            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getdetailYArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(3));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getAlbumYArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(2));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;

    }

    public ArrayList getArtistYArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);


        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(5));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getTitleByTitle(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(1));
                //   Log.e("my tag",cursor.getString(0));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getArtistByTitle(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(5));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getTitleByArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(1));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getArtistByArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(5));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getTitleByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.DATE_ADDED;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(1));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getArtistByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.DATE_ADDED;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(5));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getcoverByArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(4));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getCoverbyTitle(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(4));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getcoverByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                "track",//7
        };

        String sortOrder = MediaStore.Audio.Media.DATE_ADDED;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(4));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getMyPathByTitle(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.AudioColumns.DATA//7
        };

        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getMyPathByArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.AudioColumns.DATA//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getMyPathByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.AudioColumns.DATA//7
        };

        String sortOrder = MediaStore.Audio.Media.DATE_ADDED;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getMyRelPathByAlbum(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.Media.TITLE//7
        };

        String sortOrder = MediaStore.Audio.Media.ALBUM;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList getMyRelPathByArtist(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.Media.TITLE//7
        };

        String sortOrder = MediaStore.Audio.Media.ARTIST;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getMyRelPathByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.Media.RELATIVE_PATH//7
        };

        String sortOrder = MediaStore.Audio.Media.DATE_ADDED;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


    public ArrayList getMyIDByDate(Context context) {
        ArrayList arrayList = new ArrayList();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{
                "_id",//0
                "title",//1
                "album_id",//2
                "album",//3
                "album_id",//4
                "artist",//5
                "duration",//6
                MediaStore.Audio.Media.VOLUME_NAME//7
        };

        String sortOrder = MediaStore.Audio.Media.TITLE;
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);

        if (cursor != null && cursor.moveToNext()) {
            do {
                arrayList.add(cursor.getString(7));
            }
            while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        }
        return arrayList;
    }


//
//    @RequiresApi(api = Build.VERSION_CODES.Q)
//    public List<SongModel> getPath(Context context)
//    {
//        List<SongModel> songList= new ArrayList<>();
//
//        Uri uri= MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//        String[] projection=new String[]{
//                "_id",//0
//                "title",//1
//                "album_id",//2
//                "album",//3
//                "album_id",//4
//                "artist",//5
//                "duration",//6
//                "title"//7
//                //8
//        };
//        String sortOrder=MediaStore.Audio.Media.TITLE;
//        Cursor cursor=context.getContentResolver().query(uri,projection,null,null ,sortOrder);
//
//        if(cursor!=null&&cursor.moveToNext())
//        {
//
//            do {
//                arrayList.add(cursor.getString(1));
//            }
//            while (cursor.moveToNext());
//            if(cursor!=null)
//            {
//                cursor.close();
//            }
//        }
//        return songList;
//
//
//    }
}
