package com.music.detail.changer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.music.detail.changer.Fragments.FileFragment;
import com.music.detail.changer.Fragments.SongAdapters.SongsEditPage;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.images.Artwork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class FileRecyclerAdapter extends RecyclerView.Adapter<FileRecyclerAdapter.ViewHold> {
    Context context;
    ArrayList data;
    String path;
    byte[] art;
    LinearLayout empty;
    public int sig = 0;
    public int main_flag = 0;
    TextView pathtext;
    Bitmap songImage;
    FileFragment fileFragment;
    String root_path = "";

    public FileRecyclerAdapter(Context context, ArrayList data, String path, FileFragment fileFragment, TextView pathtext, String root_path, LinearLayout empty) {
        this.context = context;
        this.data = data;
        this.root_path = root_path;
        this.pathtext = pathtext;
        this.path = path;
        this.empty = empty;
        this.fileFragment = fileFragment;
    }

    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.file_recycler_dummy, parent, false);
        return new ViewHold(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, final int position) {
        //  Toast.makeText(context, ""+data.size(), Toast.LENGTH_SHORT).show();

        String my_title = "";
        if (main_flag == 0) {
            int temp = (data.get(position) + "").indexOf('\t');
            holder.file_text.setText((data.get(position) + "").substring(0, temp));
            my_title = (data.get(position) + "").substring(0, temp);
        } else {
            int temp = (data.get(position) + "").indexOf('\t');
            holder.file_text.setText((data.get(position) + "").substring(temp + 1));
            my_title = (data.get(position) + "").substring(temp + 1);
        }


        pathtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (path.equals(root_path)) {
                    ((MainActivity) context).refreshFileFragment();
                    return;
                }
                StringBuilder sb = new StringBuilder("" + path);

                for (int i = path.length() - 1; i >= 0; i--) {
                    if (sb.charAt(i) == '/') {
                        sb.deleteCharAt(i);
                        break;
                    } else {
                        sb.deleteCharAt(i);
                    }
                }

                path = sb.toString();


                pathtext.setText(path);
                workItAg(path);


            }
        });

        if (data.get(position).toString().contains(".mp3") || data.get(position).toString().contains(".MP3")) {


            try {
                MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                metaRetriver.setDataSource(path + "/" + my_title + "");
                art = metaRetriver.getEmbeddedPicture();
                songImage = BitmapFactory.decodeByteArray(art, 0, art.length);
                holder.imageView.setImageBitmap(songImage);


            } catch (Exception e) {
                try {
                    Uri uri = Uri.parse("android.resource://com.tagmusicapp/drawable/music");

                    songImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);


                    holder.imageView.setImageResource(R.drawable.album_icon);
                } catch (Exception e1) {
                }
            }

        } else {
            holder.imageView.setImageResource(R.drawable.ic_folder);
        }


        final String finalMy_title = my_title;
        final String finalMy_title1 = my_title;
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.R)
            @Override
            public void onClick(View view) {
                if (data.get(position).toString().contains(".mp3") || data.get(position).toString().contains(".MP3")) {
                    //  fetch(path+"/", finalMy_title +"");
                    Intent intent = new Intent(context, SongsEditPage.class);
                    intent.putExtra("songs", path + "/" + finalMy_title + "");
                    context.startActivity(intent);

                } else {
                    pathtext.setText(path + "/" + finalMy_title + "");
                    workItAg(path + "/" + finalMy_title + "");

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHold extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        TextView file_text;
        ImageView imageView;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            file_text = itemView.findViewById(R.id.file_recycler_dummy_text);
            imageView = itemView.findViewById(R.id.file_recycler_dummy_image);
            linearLayout = itemView.findViewById(R.id.file_recycler_dummy_layout);
        }
    }

    public void workItAg(String path) {


        File directory = new File(path);

        File[] files = directory.listFiles();
        data = new ArrayList();
        if (files != null) {
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {

                if (files[i].isFile()) {
                    if (files[i].getName().endsWith(".mp3") || files[i].getName().endsWith(".MP3")) {
                        Date lastModifiedd = new Date(files[i].lastModified());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                        String formattedDateString = formatter.format(lastModifiedd);
                        data.add(files[i].getName() + "\t" + formattedDateString + "");
                    }
                } else {
                    if (files[i].isDirectory()) {
                        if (files[i].getName().charAt(0) != '.') {
                            Date lastModifiedd = new Date(files[i].lastModified());
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                            String formattedDateString = formatter.format(lastModifiedd);
                            data.add(files[i].getName() + "\t" + formattedDateString + "");
                        }
                    }
                }
            }

        } else {

        }

        this.path = path;

        if (main_flag != 0) {
            sortMe();
        }

        orderMe(sig);

    }

    public void orderMe(int signal) {

        if (data.isEmpty()) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.INVISIBLE);
        }

        if (signal == 1) {
            Collections.sort(data);
            Collections.reverse(data);

            notifyDataSetChanged();
        } else {
            Collections.sort(data);


            notifyDataSetChanged();
        }
    }

    public void sortMe() {
        for (int i = 0; i < data.size(); i++) {
            String temp = data.get(i) + "";
            int seprator_index = temp.indexOf('\t');
            String new_val = temp.substring(seprator_index + 1) + "\t" + temp.substring(0, seprator_index);
            data.set(i, new_val);

        }

        notifyDataSetChanged();
        orderMe(sig);
    }


    public void fetch(String path, String filename) {

        if (true)
            return;


        File file = new File(path);

        moveFile(path + "", filename + "", Environment.getExternalStorageDirectory() + "/");


//
//                    song.save(path + "intermed");
//            File from = new File(path + ""+filename);
//            File file = new File(path);
//            file.delete();
//            File to = new File(location);
//            from.renameTo(to);


        try {
            AudioFile audioFile = AudioFileIO.read(file);
            Tag tag = audioFile.getTagOrCreateAndSetDefault();
            tag.setField(FieldKey.ARTIST, "Demo me");

//            song.save(location + "intermed");
//            File from = new File(location + "intermed");
//            File file = new File(location);
//            file.delete();
//            File to = new File(location);
//            from.renameTo(to);

            String title = tag.getFirst(FieldKey.TITLE);
            audioFile.setTag(tag);
            try {
                AudioFileIO.write(audioFile);
            } catch (CannotWriteException e) {
                Log.i("Cant", "" + e);
            }
            //          mTitleEditText.setText(title);
            //         mTitleEditText.setSelection(title.length());

            String artist = tag.getFirst(FieldKey.ARTIST);
            //        mArtistEditText.setText(artist);
            //        mArtistEditText.setSelection(artist.length());

            String album = tag.getFirst(FieldKey.ALBUM);
            //    mAlbumEditText.setText(album);
            //  mAlbumEditText.setSelection(album.length());


            String albumArtist = tag.getFirst(FieldKey.ALBUM_ARTIST);
            //   mAlbumArtistEditText.setText(albumArtist);
            // mAlbumArtistEditText.setSelection(albumArtist.length());


            String genre = tag.getFirst(FieldKey.GENRE);
            //   mGenreEditText.setText(genre);
            //  mGenreEditText.setSelection(genre.length());

            String producer = tag.getFirst(FieldKey.PRODUCER);
            //  mProducerEditText.setText(producer);
            // mProducerEditText.setSelection(mProducerEditText.length());

            String year = tag.getFirst(FieldKey.YEAR);
            // mYearEditText.setText(year);
            //  mYearEditText.setSelection(year.length());

            String track = tag.getFirst(FieldKey.TRACK);
            // mTrackEditText.setText(track);
            // mTrackEditText.setSelection(track.length());


            String totalTrack = tag.getFirst(FieldKey.TRACK_TOTAL);
            //  mTotalTrackEditText.setText(totalTrack);
            //  mTotalTrackEditText.setSelection(totalTrack.length());

            String comment = tag.getFirst(FieldKey.COMMENT);
            //  mCommentsEditText.setText(comment);
            // mCommentsEditText.setSelection(comment.length());


            List<Artwork> artwork = tag.getArtworkList();

            if (artwork.size() > 0) {
                byte[] image = artwork.get(0).getBinaryData();
                Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
//                mCardView.setCardBackgroundColor(Palette.from(bitmap).generate().getC(R.color.deep_purple));
                //       mAlbumArtImage.setImageBitmap(bitmap);
            }
        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            e.printStackTrace();

            //finish();
        }

//        MediaMetadataRetriever m = new MediaMetadataRetriever();
//        m.setDataSource(path);
//       // Bitmap thumbnail = m.getFrameAtTime();
//
//
//
//        if (Build.VERSION.SDK_INT >= 17) {
//            String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
//            String s2 = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
//            Log.e("Rotation", s+s2);
//        }

//        addMetadata(path, METADATA_KEY_ARTIST, "The Rolling Stones");
//        addMetadata(path, METADATA_KEY_ALBUM, "Sticky Fingers");
//        addMetadata(path, METADATA_KEY_TITLE, "Brown Sugar");
//        retriever.setDataSource(path);
//        assertThat(retriever.extractMetadata(METADATA_KEY_ARTIST)).isEqualTo("The Rolling Stones");
//        assertThat(retriever.extractMetadata(METADATA_KEY_ALBUM)).isEqualTo("Sticky Fingers");
//        assertThat(retriever.extractMetadata(METADATA_KEY_TITLE)).isEqualTo("Brown Sugar");


        //       Uri uri= Uri.fromFile(new File(path));

//        ContentValues content = new ContentValues();
//
//        content.put(MediaStore.Audio.Media.ARTIST,
//                "Vijay r");
//
//
//
//        ContentResolver resolver = context.getContentResolver();
//
//        Uri uri= ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,36221);
//
//        resolver.update(uri,content, null,null);

        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));

    }


    private void moveFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();


        } catch (FileNotFoundException fnfe1) {

        } catch (Exception e) {

        }

    }


}
