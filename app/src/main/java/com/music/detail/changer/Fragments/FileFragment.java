package com.music.detail.changer.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.roacult.backdrop.BackdropLayout;
import com.music.detail.changer.FileRecyclerAdapter;
import com.music.detail.changer.R;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class FileFragment extends Fragment {
    Spinner spinner, namespinner;
    public BackdropLayout backdropLayout;
    public RecyclerView recyclerView;
    ArrayList arrayList, lastModified;
    FileRecyclerAdapter fileRecyclerAdapter;
    public LinearLayout emptylayout;
    Toolbar toolbar;
    String sec_storage_text = "";
    public TextView path_text;
    String root_path;
    TextView externalstoragetext, internalstoragetext;
    LinearLayout file_storagemainlayout;
    public FileFragment f;
    AdView adView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_file, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adView = view.findViewById(R.id.filesFragmentAdview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        String[] a = {"Ascending", "Descending"};
        String[] b = {"Name", "Date Modified"};
        backdropLayout = view.findViewById(R.id.fileback_drop_layout);
        backdropLayout.setPeeckHeight(backdropLayout.getFrontLayout().getHeight() - backdropLayout.getBackLayout().getHeight());
        spinner = view.findViewById(R.id.file_spinner);
        namespinner = view.findViewById(R.id.file_spinner_name);
        toolbar = view.findViewById(R.id.filetoolbar);

        recyclerView = backdropLayout.getFrontLayout().findViewById(R.id.file_recycler);
        path_text = backdropLayout.getFrontLayout().findViewById(R.id.file_path_text);
        emptylayout = backdropLayout.getFrontLayout().findViewById(R.id.emtyfolderlayout);
        file_storagemainlayout = backdropLayout.getFrontLayout().findViewById(R.id.file_mainstorage_layout);
        externalstoragetext = backdropLayout.getFrontLayout().findViewById(R.id.file_externalstorage_text);
        internalstoragetext = backdropLayout.getFrontLayout().findViewById(R.id.file_internalstorage_text);


//        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                return true;
//            }
//        });

        setUpFrontPage();

        ArrayList storagepaths = getRemovableStorageRoots(getActivity());

        if (storagepaths.isEmpty()) {
            externalstoragetext.setVisibility(View.GONE);
        } else {
            sec_storage_text = storagepaths.get(0) + "/";
        }

        internalstoragetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (permissionGranted()) {
                    root_path = Environment.getExternalStorageDirectory().toString();
                    setOtherPages();
                    workIt(root_path);
                } else {
                    requestPermission();
                }
            }
        });

        externalstoragetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (permissionGranted()) {
                    root_path = sec_storage_text;
                    setOtherPages();
                    workIt(root_path);
                } else {
                    requestPermission();
                }
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy, a);
        spinner.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy, b);
        namespinner.setAdapter(adapter2);

        f = new FileFragment();


        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getTitle().toString().equals("Sort by")) {
                    backdropLayout.open();
                    return true;
                } else {
                    return false;
                }
            }
        });


    }


    private boolean permissionGranted() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            root_path = Environment.getExternalStorageDirectory().toString();
            workIt(root_path);
            setOtherPages();
        }
    }


    public void workIt(String path) {

        //   Log.d("Files", "Path: " + path);
        File directory = new File(path);
        path_text.setText(path);
        File[] files = directory.listFiles();
        arrayList = new ArrayList();
        lastModified = new ArrayList();
        //  Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++) {
            //   Log.d("Files", "FileName:" + Uri.fromFile(new File(files[i]+"")));
            if (files[i].isFile()) {
                if (files[i].getName().endsWith(".mp3") || files[i].getName().endsWith(".MP3")) {

                    Date lastModifiedd = new Date(files[i].lastModified());
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                    String formattedDateString = formatter.format(lastModifiedd);
                    arrayList.add(files[i].getName() + "\t" + formattedDateString + "");

                }
            } else {
                if (files[i].isDirectory()) {
                    if (files[i].getName().charAt(0) != '.') {
                        Date lastModifiedd = new Date(files[i].lastModified());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                        String formattedDateString = formatter.format(lastModifiedd);
                        arrayList.add(files[i].getName() + "\t" + formattedDateString + "");

                    }
                }
            }


        }

        fileRecyclerAdapter = new FileRecyclerAdapter(getActivity(), arrayList, path, f, path_text, root_path, emptylayout);

        Collections.sort(arrayList);

        if (root_path.equals(path)) {

            recyclerView.setAdapter(fileRecyclerAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setItemViewCacheSize(arrayList.size());
        } else {
            fileRecyclerAdapter.notifyDataSetChanged();
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    fileRecyclerAdapter.orderMe(0);
                    fileRecyclerAdapter.sig = 0;
                } else {
                    fileRecyclerAdapter.orderMe(1);
                    fileRecyclerAdapter.sig = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        namespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    if (fileRecyclerAdapter.main_flag != 0) {
                        fileRecyclerAdapter.sortMe();
                        fileRecyclerAdapter.main_flag = 0;
                    }

                } else {
                    if (fileRecyclerAdapter.main_flag != 1) {
                        fileRecyclerAdapter.sortMe();
                        fileRecyclerAdapter.main_flag = 1;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    public static ArrayList getRemovableStorageRoots(Context context) {
        File[] roots = context.getExternalFilesDirs("external");
        ArrayList<File> rootsArrayList = new ArrayList<>();
        for (int i = 0; i < roots.length; i++) {
            if (roots[i] != null) {
                String path = roots[i].getPath();
                int index = path.lastIndexOf("/Android/data/");
                if (index > 0) {
                    path = path.substring(0, index);
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath())) {
                        rootsArrayList.add(new File(path));
                    }
                }
            }
        }

        //   Log.e("MyPaths",rootsArrayList+"");
//
//        roots = new File[rootsArrayList.size()];
//        rootsArrayList.toArray(roots);
        return rootsArrayList;
    }


    void setUpFrontPage() {
        path_text.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        file_storagemainlayout.setVisibility(View.VISIBLE);
    }

    void setOtherPages() {
        path_text.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        file_storagemainlayout.setVisibility(View.GONE);
    }

}

