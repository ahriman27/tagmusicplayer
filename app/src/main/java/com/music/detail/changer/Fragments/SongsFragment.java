package com.music.detail.changer.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.roacult.backdrop.BackdropLayout;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.music.detail.changer.Fragments.SongAdapters.SongAdapter;
import com.music.detail.changer.Fragments.SongAdapters.SongLoader;
import com.music.detail.changer.MainActivity;
import com.music.detail.changer.R;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.images.Artwork;
import org.jaudiotagger.tag.images.ArtworkFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;


public class SongsFragment extends Fragment implements SearchView.OnQueryTextListener {

    public SongAdapter adapter;
    public FastScrollRecyclerView recyclerView;
    public SearchView searchView;
    public Toolbar toolbar, toolbar2;
    Bitmap myLoadedBitmap;
    int spinnerFlag = 0;
    int adflag = 0;
    ImageView closeSearch, backimageselected;
    public FloatingActionButton floatingActionButton;
    Spinner sortSongOrder, artistSpinner;
    public BackdropLayout backdropLayout;
    public TextView numbsItemsSelected;
    public LinearLayout sortingLayout, recyclerSelectToolbarLayout, searchViewLayout;
    String[] a = {"Title", "Artist", "Data Added"};
    String[] b = {"Ascending", "Descending"};
    ArrayList allDataList;
    ArrayList songsList;
    LinearLayout myprolayout;
    ImageView mygifview;
    ProgressBar myprobar;
    ArrayList albumList;
    ArrayList detailList;
    String sdcardStorage;
    ArrayList pathList;
    public int kundle = 0, kundle2 = 0;
    AdView adView;
    RewardedAd rewardedAd;

    public SongsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_songs, container, false);

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });


        adView = view.findViewById(R.id.songsFragmentAdview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        rewardedAd = new RewardedAd(getContext(),
                "ca-app-pub-8305515768751170/6096021980");


        setHasOptionsMenu(true);

        backdropLayout = view.findViewById(R.id.back_drop_layout);
        // backdropLayout.setPeeckHeight(backdropLayout.getFrontLayout().getHeight()-backdropLayout.getBackLayout().getHeight());
        artistSpinner = view.findViewById(R.id.song_sort_spinner);
        sortSongOrder = view.findViewById(R.id.song_order_spinner);
        sortingLayout = backdropLayout.findViewById(R.id.song_sort_backLayout);
        floatingActionButton = view.findViewById(R.id.floatingActionButton);

        final ArrayAdapter<String> adapterSong = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy, a);
        artistSpinner.setAdapter(adapterSong);

        recyclerSelectToolbarLayout = view.findViewById(R.id.recycler_items);


        myprobar = backdropLayout.getFrontLayout().findViewById(R.id.progress_bar_songfrag);
        myprolayout = backdropLayout.getFrontLayout().findViewById(R.id.songfrag_progress_layout);
        mygifview = backdropLayout.getFrontLayout().findViewById(R.id.gifimagesongfrag);

        numbsItemsSelected = view.findViewById(R.id.numbsSongsSelectd);

        ImageView bacFromSelected = view.findViewById(R.id.backFromSelectedItems);
        bacFromSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((MainActivity) getActivity()).refreshSongsFragment();

            }
        });

        //     Toast.makeText(getActivity(), "path"+new  SongLoader().getPath(getActivity()).size(), Toast.LENGTH_SHORT).show();
//Log.i("path",new  SongLoader().getMyPathByTitle(getActivity()).toString());


        //adviews


        searchViewLayout = view.findViewById(R.id.searchViewLayout);
        // closeSearch=view.findViewById(R.id.cloaseSearch);
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), R.layout.spinnerdummy, b);
        sortSongOrder.setAdapter(adapter2);
        toolbar = view.findViewById(R.id.toolbar);
        searchView = view.findViewById(R.id.song_search_view);
        //    backimageselected=toolbar.findViewById(R.id.backFromSelectedItems);
        recyclerView = view.findViewById(R.id.songs_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getDataNew(getActivity());

//        backimageselected.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((MainActivity)getActivity()).refreshSongsFragment();
//            }
//        });

        sortSongOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0 && spinnerFlag == 1) {
                    kundle2 = 0;
                    getData(getActivity());
                    adapter.notifyDataSetChanged();
                } else if (position == 1) {
                    kundle2 = 1;
                    getData(getActivity());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        artistSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0 && spinnerFlag == 1) {
                    kundle = 0;
                    getData(getActivity());
                    adapter.notifyDataSetChanged();
                } else if (position == 1) {
                    kundle = 1;
                    getData(getActivity());
                    adapter.notifyDataSetChanged();
                } else if (position == 2) {
                    kundle = 2;
                    getData(getActivity());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ViewGroup viewGroup = view.findViewById(android.R.id.content);
//                View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.custo_dialog_layout, viewGroup, false);
//
//
//                //Now we need an AlertDialog.Builder object
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
//
//                //setting the view of the builder to our custom view that we already inflated
//                builder1.setView(dialogView);
//
//                //finally creating the alert dialog and displaying it
//                final AlertDialog alertDialog = builder1.create();
//               alertDialog.show();


                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custo_dialog_layout);
                dialog.show();
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                final RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
                    @Override
                    public void onRewardedAdLoaded() {
                        // Ad successfully loaded.
                        if (rewardedAd.isLoaded()) {
                            dialog.dismiss();

                            RewardedAdCallback adCallback = new RewardedAdCallback() {
                                @Override
                                public void onRewardedAdOpened() {
                                    // Ad opened.
                                    dialog.dismiss();
                                }

                                @Override
                                public void onRewardedAdClosed() {
                                    // Ad closed.
//                                    Toast.makeText(getActivity(), "Watch full ad to continue.", Toast.LENGTH_SHORT).show();
//                                    ((MainActivity) getActivity()).refreshSongsFragment();
//                                    dialog.dismiss();

                                    if (adflag == 1) {
                                        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                                        i.setType("image/*");
                                        startActivityForResult(i, 8);
                                    } else {
                                        Toast.makeText(getActivity(), "Watch full ad to continue.", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onUserEarnedReward(@NonNull RewardItem reward) {

                                    dialog.dismiss();

                                    adflag = 1;


                                }

                                @Override
                                public void onRewardedAdFailedToShow(AdError adError) {
                                    dialog.dismiss();
                                    // Ad failed to display.
                                    //          Toast.makeText(getContext(), "Please try Again !!", Toast.LENGTH_SHORT).show();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setMessage("Ad Load Failed,Please Try again! ")
                                            .setCancelable(false)
                                            .setIcon(R.drawable.vector_exit)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //   dialog.dismiss();
                                                }
                                            });

                                    AlertDialog alert = builder.create();

                                    alert.show();
                                    alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                                    ((MainActivity) getActivity()).refreshSongsFragment();
                                }
                            };
                            rewardedAd.show(getActivity(), adCallback);
                        } else {
                            dialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("Ad Load Failed,Please Try again! ")
                                    .setCancelable(false)
                                    .setIcon(R.drawable.vector_exit)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //       dialog.dismiss();
                                        }
                                    });

                            AlertDialog alert = builder.create();

                            alert.show();
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            ((MainActivity) getActivity()).refreshSongsFragment();

                        }
                        //           Toast.makeText(getContext(), "loaded", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onRewardedAdFailedToLoad(LoadAdError adError) {
                        // Ad failed to load.
                        dialog.dismiss();

                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Please try after some time with strong internet connection !!")
                                .setCancelable(false)
                                .setIcon(R.drawable.vector_exit)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });

                        AlertDialog alert = builder.create();

                        alert.show();
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));

                    }
                };
                rewardedAd.loadAd(new PublisherAdRequest.Builder().build(), adLoadCallback);


                //         Toast.makeText(getContext(), "Floating", Toast.LENGTH_SHORT).show();

            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                switch (menuItem.getItemId()) {

                    case R.id.toobar_refresh_id: {
                        ((MainActivity) getActivity()).refreshSongsFragment();
                        return true;
                    }
                    case R.id.toolbar_search_id: {

                        searchViewLayout.setVisibility(View.VISIBLE);
                        searchView.setOnQueryTextListener(SongsFragment.this);
                        return true;
                    }
                    case R.id.toolbar_sort_id: {

                        backdropLayout.open();
                        sortingLayout.setVisibility(View.VISIBLE);
                        //  Toast.makeText(getContext(), "Sorting", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });


        spinnerFlag = 1;

//
//        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
//        Boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();
//
//        if(isSDSupportedDevice && isSDPresent)
//        {
//            // yes SD-card is present
//        }
//        else
//        {
//            // Sorry
//        }


        return view;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (newText == null || newText.trim().isEmpty()) {
            adapter.setFilter(songsList, detailList, albumList, pathList);
            return false;
        }

        newText = newText.toLowerCase();
        final ArrayList filteredsongList = new ArrayList<>();
        final ArrayList filteredartistList = new ArrayList<>();
        final ArrayList filteredcoverList = new ArrayList<>();
        final ArrayList filteredpathList = new ArrayList<>();
        for (int i = 0; i < songsList.size(); i++) {
            final String title = songsList.get(i).toString().toLowerCase();
            final String artist = albumList.get(i).toString().toLowerCase();


            if ((title.contains(newText)) || (artist.contains(newText))) {
                filteredsongList.add(songsList.get(i));
                filteredcoverList.add(albumList.get(i));
                filteredartistList.add(detailList.get(i));
                filteredpathList.add(pathList.get(i));
            }
        }
        adapter.setFilter(filteredsongList, filteredartistList, filteredcoverList, filteredpathList);
        return true;
    }


    public class loadSong extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (getActivity() != null) {

//                getDataNew(getActivity());

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            recyclerView.setAdapter(adapter);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void getData(Context context) {
        SongLoader songLoader = new SongLoader();
        allDataList = new ArrayList();
        if (kundle == 0) {
            adapter.title = songLoader.getTitleByTitle(context);
            adapter.cover = songLoader.getCoverbyTitle(context);
            adapter.artist = songLoader.getArtistByTitle(context);
            adapter.pathList = songLoader.getMyPathByTitle(context);

            if (kundle2 == 1) {
                Collections.reverse(adapter.title);
                Collections.reverse(adapter.cover);
                Collections.reverse(adapter.artist);
                Collections.reverse(adapter.pathList);
            }
        } else if (kundle == 1) {
            adapter.title = songLoader.getTitleByArtist(context);
            adapter.cover = songLoader.getcoverByArtist(context);
            adapter.artist = songLoader.getArtistByArtist(context);
            adapter.pathList = songLoader.getMyPathByArtist(context);

            if (kundle2 == 1) {
                Collections.reverse(adapter.title);
                Collections.reverse(adapter.cover);
                Collections.reverse(adapter.artist);
                Collections.reverse(adapter.pathList);
            }
        } else if (kundle == 2) {
            adapter.title = songLoader.getTitleByDate(context);
            adapter.cover = songLoader.getcoverByDate(context);
            adapter.artist = songLoader.getArtistByDate(context);
            adapter.pathList = songLoader.getMyPathByDate(context);

            if (kundle2 == 1) {
                Collections.reverse(adapter.title);
                Collections.reverse(adapter.cover);
                Collections.reverse(adapter.artist);
                Collections.reverse(adapter.pathList);
            }
        }


        //  adapter= new SongAdapter(getActivity(), songsList,albumList,detailList);

    }

    public void getDataNew(Context context) {
        SongLoader songLoader = new SongLoader();

        songsList = songLoader.getTitleByTitle(context);
        albumList = songLoader.getCoverbyTitle(context);
        detailList = songLoader.getArtistByTitle(context);
        pathList = songLoader.getMyPathByTitle(context);

        adapter = new SongAdapter(getActivity(), songsList, albumList, detailList, pathList, recyclerSelectToolbarLayout,
                floatingActionButton, numbsItemsSelected, toolbar);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(songsList.size());

    }


    public String setMp3AlbumArt(File SourceFile, byte[] bytes) {
        String error = null;
        try {
            MP3File musicFile = (MP3File) AudioFileIO.read(SourceFile);
            AbstractID3v2Tag tag = musicFile.getID3v2Tag();
            String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("png")};
            if (tag == null) {
                //       Log.e("TagNull","");
            }

            if (tag != null) {
                Artwork artwork = null;
                try {
                    artwork = ArtworkFactory.createArtworkFromFile(SourceFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    error = e.getMessage();
                }
                if (artwork != null) {
                    //          Log.e("art found","found"+tag.getFields(FieldKey.COVER_ART));
                    artwork.setBinaryData(bytes);
                    tag.deleteArtworkField();
                    artwork.setMimeType(mimeType[0]);
                    tag.setField(artwork);

                    musicFile.setTag(tag);
                    musicFile.commit();
                } else {
                    //         Log.e("art null","art null");
                    artwork.setBinaryData(bytes);
                    tag.addField(artwork);
                    tag.setField(artwork);
                    musicFile.setTag(tag);
                    musicFile.commit();
                }
            }

        } catch (Exception e) {
            error = e.getMessage();
            //      Log.e("Myerror",error);
        }
        return error;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 8) {
            if (data == null)
                return;
            Uri uri = data.getData();
            try {
                myLoadedBitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                int myflag = 1;


                SongsFragment.MyHelper myHelper = new MyHelper();
                myHelper.execute();
            } catch (Exception e) {

            }
//            Bitmap tmbitmap=myLoadedBitmap;
//            ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
//            tmbitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
//            byte[] byteArray=byteArrayOutputStream.toByteArray();
//            File file = new File(adapter.selectedPathList.get(0)+"");

//            setMp3AlbumArt(file ,byteArray);


        }

    }

    void setDetails(int type) {

        for (int i = 0; i < adapter.selectedPathList.size(); i++) {

            int index = adapter.selectedPathList.get(i).toString().lastIndexOf('/');

            ArrayList rootDir = getRemovableStorageRoots(getActivity());

            //    Log.e("my selected paths", adapter.selectedPathList+ "");

            if (!rootDir.isEmpty() && adapter.selectedPathList.get(i).toString().contains(rootDir.get(0).toString())) {

                sdcardStorage = rootDir.get(0).toString();

                moveFile(i, adapter.selectedPathList.get(i).toString().substring(0, index + 1), adapter.selectedPathList.get(i).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/", type);

                getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(adapter.selectedPathList.get(i) + ""))));


            } else {

                //        moveFile(pathList.get(i).toString().substring(0, index + 1), pathList.get(i).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/");


                //   Uri uri= Uri.parse("file:///"+ pathList.get(0));

                File file = new File(adapter.selectedPathList.get(i) + "");

//            File file = new File(uri+"");

                if (type == 1) {
                    try {
                        AudioFile audioFile = AudioFileIO.read(file);

                        Tag tag = audioFile.getTagOrCreateAndSetDefault();


                        audioFile.setTag(tag);

                        audioFile.commit();
                        try {
                            AudioFileIO.write(audioFile);

                        } catch (CannotWriteException e) {
                            //         Log.i("Cant", "" + e);
                        }


                    } catch (Exception e) {

                    }
                } else {

                    //     Log.e("Local file","");

                    Bitmap tmbitmap = myLoadedBitmap;

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    // myLoadedBitmap.recycle();
                    //       Log.e("Local file byte",""+byteArray);

//
//                    Uri imageUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id));
//                    Bitmap scaledbmp = decodeSampledBitmapFromResource(imageUri, 300, 300);
//                    if (scaledbmp != null) {
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        scaledbmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] byteArray = stream.toByteArray();
//

                    setMp3AlbumArt(file, byteArray);
                }

                getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(adapter.selectedPathList.get(i) + ""))));
            }

        }
    }

    private void moveFile(int myRelIndex, String inputPath, String inputFile, String outputPath, int type) {


        InputStream in = null;
        OutputStream out = null;

        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                //   Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();

            File file = new File(outputPath + "/" + inputFile);

            try {

                if (type == 1) {

                    AudioFile audioFile = AudioFileIO.read(file);

                    Tag tag = audioFile.getTagOrCreateAndSetDefault();


                    audioFile.setTag(tag);

                    audioFile.commit();
                    try {
                        AudioFileIO.write(audioFile);

                    } catch (CannotWriteException e) {
                        //       Log.i("Cant", "" + e);
                    }
                } else {
                    Bitmap tmbitmap = myLoadedBitmap;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    // myLoadedBitmap.recycle();
                    setMp3AlbumArt(file, byteArray);
                }


                //  String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("saf", MODE_PRIVATE);
                String mySafUri = sharedPreferences.getString("saf", "");

//                ArrayList rootS=getRemovableStorageRoots(getApplicationContext());
//
//                StringBuilder sb=new StringBuilder("");
//
//                String comtext=inputPath+inputFile;
//                int comindex=comtext.charAt('/');
//
//                String myFinalPath=(inputPath).substring(rootS.get(0).toString().length()+1);
//
//                for(int i=0;i<myFinalPath.length()-1;i++)
//                {
//                    if(myFinalPath.charAt(i)=='/')
//                    {
//                        sb.append("%2F");
//                    }
//                    else if(myFinalPath.charAt(i)==' ')
//                    {
//                        sb.append("%20");
//                    }
//                    else if(myFinalPath.charAt(i)=='+')
//                    {
//                        sb.append("%2B");
//                    }
//                    else if(myFinalPath.charAt(i)=='*')
//                    {
//                        sb.append("%C3%B7");
//                    }
//                    else if(myFinalPath.charAt(i)=='=')
//                    {
//                        sb.append("%3D");
//                    }
//                    else if(myFinalPath.charAt(i)=='%')
//                    {
//                        sb.append("%25");
//                    }
//                    else if(myFinalPath.charAt(i)=='&')
//                    {
//                        sb.append("%26");
//                    }
//                    else if(myFinalPath.charAt(i)=='@')
//                    {
//                        sb.append("%40");
//                    }
//                    else if(myFinalPath.charAt(i)=='$')
//                    {
//                        sb.append("%24");
//                    }
//                    else if(myFinalPath.charAt(i)=='#')
//                    {
//                        sb.append("%23");
//                    }
//                    else if(myFinalPath.charAt(i)=='₹')
//                    {
//                        sb.append("%E2%82%B9");
//                    }
//                    else
//                    {
//                        sb.append(myFinalPath.charAt(i));
//                    }
//                }
//
//                String pathg=mySafUri+sb.toString();
//
//                Log.e("MyFinalPath",myFinalPath);
//                Log.e("Roots",rootS+"");
//                Log.e("MySaf",mySafUri+"");
//                Log.e("My Sb",sb+"");
//                Log.e("My inputfilee",inputFile+"");
//                Log.e("MyConvSAF",mySafUri+sb.toString());

                //      grantUriPermission(getPackageName(), Uri.parse(mySafUri), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );

//                grantUriPermission(getPackageName(), Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                final int takeFlags =   (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                getContentResolver().takePersistableUriPermission(Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), takeFlags);

//                 DocumentFile pickedDirr = DocumentFile.fromTreeUri(getApplicationContext(), Uri.parse("/3863-6134/audio/media/70052"));
//                  content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
                // DocumentFile pickedDir = pickedDirr.createFile(mimeType[0], inputFile);

                DocumentFile pickedDirr = findMySafDir(inputPath.substring(sdcardStorage.length()) + "", Uri.parse(mySafUri), inputFile);

                moveFileBackBySaf(outputPath, inputFile, pickedDirr);


                //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
                //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit

//                SafHelper safHelper=new SafHelper(getApplicationContext(),"/storage/3863-6134/",1);
//                safHelper.requestPermissions(Song_Edits.this);

            } catch (Exception e) {
                //       Log.e("errrrror", e + "");
            }


        } catch (FileNotFoundException fnfe1) {
            //     Log.e("tagg", fnfe1.getMessage());
            //      Toast.makeText(getActivity(), "Something went Wrong!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            //      Log.e("tag", e.getMessage());
            //     Toast.makeText(getActivity(), "Something went Wrong!!", Toast.LENGTH_SHORT).show();
        }

        Objects.requireNonNull(getContext()).sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(adapter.selectedPathList.get(myRelIndex) + ""))));

    }

    DocumentFile findMySafDir(String relative_path, Uri uri, String filename) {

        ArrayList pathFragments = new ArrayList();

        StringBuilder sb = new StringBuilder("");

        for (int i = 0; i < relative_path.length(); i++) {
            if (relative_path.charAt(i) == '/') {
                pathFragments.add(sb + "");
                sb = new StringBuilder("");
            } else {
                sb.append(relative_path.charAt(i));
            }
        }

        DocumentFile documentFile = DocumentFile.fromTreeUri(getContext(), uri);
        //  DocumentFile[] docs=documentFile.listFiles();
        DocumentFile[] docs;
        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

        String pathUnit = "";
        for (int j = 0; j < pathFragments.size(); j++) {
            pathUnit = pathFragments.get(j) + "";

            docs = documentFile.listFiles();

            for (int i = 0; i < docs.length; i++) {
                if (docs[i].getName().toString().equals(pathUnit)) {
                    documentFile = docs[i];
                    //     Log.e("Done at",""+pathUnit);
                    break;
                } else {
                    //      Log.e("FinderError","Lg gye at"+pathUnit);
                }
            }

        }


        DocumentFile ret_doc_fi = documentFile.findFile(filename);

        ret_doc_fi.delete();

        DocumentFile ret_doc_file = documentFile.createFile(mimeType[0], filename);

        return ret_doc_file;
    }

    private void moveFileBackBySaf(String inputPath, String inputFile, DocumentFile sdDir) {

        //   Log.e("Moving file back", "" + inputFile);

        //   Log.e("inputpath", "" + inputPath);
        //   Log.e("inputfile", "" + inputFile);
        //   Log.e("inputfile", "" + completePath);
        //     Log.e("outputpath",""+outputPath);

        InputStream in = null;
        OutputStream out = null;
        try {

            String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3");
            //   Log.e("mime:", mimeType);

//            //create output directory if it doesn't exist
//            File dir = new File (outputPath);
//            if (!sdDir.exists())
//            {
//                sdDir.getParentFile().createFile(mimeType,"newfile.mp3");
//            }


            in = new FileInputStream(inputPath + inputFile);


            out = getContext().getContentResolver().openOutputStream(sdDir.getUri());

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                //     Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();


        } catch (FileNotFoundException fnfe1) {
            //   Log.e("tagg", fnfe1.getMessage());
        } catch (Exception e) {
            //   Log.e("tag", e.getMessage());
        }

    }

    public static ArrayList getRemovableStorageRoots(Context context) {
        File[] roots = context.getExternalFilesDirs("external");
        ArrayList<File> rootsArrayList = new ArrayList<>();
        for (int i = 0; i < roots.length; i++) {
            if (roots[i] != null) {
                String path = roots[i].getPath();
                int index = path.lastIndexOf("/Android/data/");
                if (index > 0) {
                    path = path.substring(0, index);
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath())) {
                        rootsArrayList.add(new File(path));
                    }
                }
            }
        }

        ////     Log.e("MyPaths", rootsArrayList + "");
//
//        roots = new File[rootsArrayList.size()];
//        rootsArrayList.toArray(roots);
        return rootsArrayList;
    }

    class MyHelper extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            setDetails(0);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myprolayout.setVisibility(View.VISIBLE);
            myprobar.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //     Toast.makeText(getContext(), "Details Saved", Toast.LENGTH_SHORT).show();

            Glide.with(getActivity()).load(R.drawable.check).into(mygifview);

            myprobar.setVisibility(View.INVISIBLE);
            mygifview.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {


                    try {
                        Thread.sleep(2500);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                myprolayout.setVisibility(View.GONE);
                                ((MainActivity) getActivity()).refreshSongsFragment();
                            }
                        });


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }


    }
}

