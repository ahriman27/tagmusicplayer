package com.music.detail.changer.Fragments.SongAdapters;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.documentfile.provider.DocumentFile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.music.detail.changer.MainActivity;
import com.music.detail.changer.R;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.images.Artwork;
import org.jaudiotagger.tag.images.ArtworkFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class SongsEditPage extends AppCompatActivity {
    private static final int GALLER_CODE = 1;
    public ImageView backButton, addImageFromGallery;
    LinearLayout linearLayout1, backButton1, doneButton, progressDialog;
    TextView bitRate, songPath, songName, songNameOnToolbar;
    AppCompatImageView songImage;
    ImageView backFromToolbar, doneFromToolbar, moreFromToolbar;
    String title = "";
    Bitmap myLoadedBitmap;
    public int coverflag = 0;
    int flag = 0, flagT = 0;
    TextInputEditText songTitle, songYears, songGenere, songsAlbumArtist, songsAlbumName, songsLyrics, songsComments, songArtist;
    String etx_songYear, etx_songTitle, etx_songArtist, etx_songGenere, etx_songalbumName, etxAlbumartistName, etx_songComments;
    public Tag tag;
    public AudioFile audioFile;
    File file;
    //  Bitmap mycover;
    Uri imageURI;
    byte[] inputData;
    String myPath;
    ImageView More_icon2, gifview;
    MediaPlayer mediaPlayer;
    String sdcardStorage;
    BottomSheetDialog_songs songs;
    public TextInputLayout til1, til2, til3, tile4, tile5, tile6, tile7, tile8;
    ProgressBar progressBar;
    private InterstitialAd mInterstitialAd;
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_edit_page);
        //getSupportActionBar().hide();
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });

        adView = findViewById(R.id.songsEditPageAdview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        songs = new BottomSheetDialog_songs();
        mediaPlayer = new MediaPlayer();
        progressDialog = findViewById(R.id.songsEditFragment_progress_layout);

        songTitle = findViewById(R.id.songTitle);
        songArtist = findViewById(R.id.songArtist);
        songGenere = findViewById(R.id.songGenere);
        songsAlbumName = findViewById(R.id.songAlbum);
        songsAlbumArtist = findViewById(R.id.songAlbumArtist);
        songsComments = findViewById(R.id.songComments);
        songYears = findViewById(R.id.songYear);
        songPath = findViewById(R.id.songPath);
        songsLyrics = findViewById(R.id.songLyrics);

        bitRate = findViewById(R.id.songBitrate);
        songImage = findViewById(R.id.songCover);
        songName = findViewById(R.id.songName);
        backFromToolbar = findViewById(R.id.backFromToolbar);
        // moreFromToolbar = findViewById(R.id.moreFromToolbar);
        doneFromToolbar = findViewById(R.id.doneFromToolbar);
        songNameOnToolbar = findViewById(R.id.song_name_on_toolbar);
        addImageFromGallery = findViewById(R.id.addImageFromGallery);
        progressBar = findViewById(R.id.progress_bar_songEditPage);
        gifview = findViewById(R.id.gifimageSongEditPage);
        til1 = findViewById(R.id.textfield1);
        til2 = findViewById(R.id.textfield2);
        til3 = findViewById(R.id.textfield3);
        tile4 = findViewById(R.id.textfield4);
        tile5 = findViewById(R.id.textfield5);
        tile6 = findViewById(R.id.textfield6);
        tile7 = findViewById(R.id.textfield7);
        tile8 = findViewById(R.id.textfield8);
        More_icon2 = findViewById(R.id.more_icon2);
        More_icon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playPauseSong();
            }
        });
        mInterstitialAd = new InterstitialAd(SongsEditPage.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8305515768751170/6479165367");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());



        addImageFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                startActivityForResult(i,7);

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    //       Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded()

                    {
                        // Code to be executed when an ad finishes loading.
                        mInterstitialAd.show();

                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        // Code to be executed when an ad request fails.
                    }

                    @Override
                    public void onAdOpened() {
                        // Code to be executed when the ad is displayed.
                    }

                    @Override
                    public void onAdClicked() {
                        // Code to be executed when the user clicks on an ad.
                    }

                    @Override
                    public void onAdLeftApplication() {
                        // Code to be executed when the user has left the app.
                    }

                    @Override
                    public void onAdClosed()
                    {
                        // Code to be executed when the interstitial ad is closed.
//                        Intent i=new Intent(Intent.ACTION_GET_CONTENT);
//                        i.setType("image/*");
//                        startActivityForResult(i,7);

                    }
                });


            }
        });


//        mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                } else {
//                    Log.d("TAG", "The interstitial wasn't loaded yet.");
//                }
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                // Code to be executed when an ad request fails.
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when the ad is displayed.
//            }
//
//            @Override
//            public void onAdClicked() {
//                // Code to be executed when the user clicks on an ad.
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when the interstitial ad is closed.
//            }
//        });



        myPath = getIntent().getExtras().getString("songs");
        fetch(myPath);
        songPath.setText(myPath);
        //  mycover = (Bitmap)getIntent().getExtras().get("cover");
//        Toast.makeText(this, "cover"+mycover, Toast.LENGTH_SHORT).show();


        try {
            assert myPath != null;
            //  Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), getImage(Long.parseLong(mycover)));


            try {
                MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                metaRetriver.setDataSource(myPath + "");
                byte[] art = metaRetriver.getEmbeddedPicture();
                Bitmap Image = BitmapFactory.decodeByteArray(art, 0, art.length);
                songImage.setImageBitmap(Image);


            } catch (Exception e) {

                try {
                    Uri uri = Uri.parse("android.resource://com.tagmusicapp/drawable/music");
                    //   Log.i("bitme", "" + "bitmap");
                    Bitmap Image = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    songImage.setImageBitmap(Image);
                } catch (Exception e1) {

                }
            }


        } catch (Exception e) {

            songImage.setImageResource(R.drawable.songs_icon);

        }
//
        // Toast.makeText(this, "path "+arrayList.size(), Toast.LENGTH_SHORT).show();
        final Toolbar toolbar = findViewById(R.id.songs_edit_toolbar);

        //    Log.e("My Path", "" + myPath);

        backButton1 = findViewById(R.id.back_listener);
        backButton = findViewById(R.id.backFromToolbar);
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);

        linearLayout1 = findViewById(R.id.back_button_layout);


        final LinearLayout linearLayout = findViewById(R.id.layout_to_hide);

        collapsingToolbar.setTitleEnabled(false);

        AppBarLayout mAppBarLayout = findViewById(R.id.app_bar);

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == -collapsingToolbar.getHeight() + toolbar.getHeight()) {
                    if (title.equals("")) {
                        songNameOnToolbar.setText("<unknown title>");
                    } else {
                        songNameOnToolbar.setText(title);
                    }

                    linearLayout.setVisibility(View.VISIBLE);
                    //toolbar is collapsed here
                    //write your code here
                } else {

                    linearLayout.setVisibility(View.INVISIBLE);
                    toolbar.setTitle("");
                    linearLayout1.setVisibility(View.VISIBLE);

                }


            }
        });

        backFromToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        doneFromToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (songTitle.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    til1.setError("Empty field");
                    songTitle.setFocusable(true);
                    return;
                }
                if (songArtist.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    til2.setError("Empty field");
                    songArtist.setFocusable(true);
                    return;
                }
                if (songGenere.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    til3.setError("Empty field");
                    songGenere.setFocusable(true);
                    return;
                }
//                if (songsLyrics.getText().toString().equals("")) {
//                    //  Log.i("Field","Empty");
//                    tile4.setError("Empty field");
//                    songsLyrics.setFocusable(true);
//                    return;
//                }
                if (songsComments.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    tile5.setError("Empty field");
                    songsComments.setFocusable(true);
                    return;
                }
                if (songsAlbumName.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    tile6.setError("Empty field");
                    songsAlbumName.setFocusable(true);
                    return;
                }
                if (songsAlbumArtist.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    tile7.setError("Empty field");
                    songsAlbumArtist.setFocusable(true);
                    return;
                }
                if (songYears.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    tile8.setError("Empty field");
                    songYears.setFocusable(true);
                    return;
                }


                coverflag = 0;
                MyHelper myHelper = new MyHelper();
                myHelper.execute();
            }
        });

//        moreFromToolbar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BottomSheetDialog_songs bottomSheetDialog1 = new BottomSheetDialog_songs();
//                bottomSheetDialog1.show(getSupportFragmentManager(), bottomSheetDialog1.getTag());
//            }
//        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                // startActivity(new Intent(getApplicationContext(),SongsFragment.class));

            }
        });
        mAppBarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.back_arrow) {
                    //       Toast.makeText(SongsEditPage.this, "clicked", Toast.LENGTH_SHORT).show();
                }
            }
        });
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //    Toast.makeText(SongsEditPage.this, "backIsWorkin", Toast.LENGTH_SHORT).show();
            }
        });
        doneButton = findViewById(R.id.done_listener);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (songTitle.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    til1.setError("Empty field");
                    songTitle.setFocusable(true);
                    return;
                }
                if (songArtist.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    til2.setError("Empty field");
                    songArtist.setFocusable(true);
                    return;
                }
                if (songGenere.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    til3.setError("Empty field");
                    songGenere.setFocusable(true);
                    return;
                }
//                if (songsLyrics.getText().toString().equals("")) {
//                    //   Log.i("Field","Empty");
//                    tile4.setError("Empty field");
//                    songsLyrics.setFocusable(true);
//                    return;
//                }
                if (songsComments.getText().toString().equals("")) {
                    //  Log.i("Field","Empty");
                    tile5.setError("Empty field");
                    songsComments.setFocusable(true);
                    return;
                }
                if (songsAlbumName.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    tile6.setError("Empty field");
                    songsAlbumName.setFocusable(true);
                    return;
                }
                if (songsAlbumArtist.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    tile7.setError("Empty field");
                    songsAlbumArtist.setFocusable(true);
                    return;
                }
                if (songYears.getText().toString().equals("")) {
                    //   Log.i("Field","Empty");
                    tile8.setError("Empty field");
                    songYears.setFocusable(true);
                    return;
                }


                coverflag = 0;
                MyHelper myHelper = new MyHelper();
                myHelper.execute();

//                etx_songTitle= songTitle.getText().toString().trim();
//                etx_songArtist= songArtist.getText().toString().trim();
//                etx_songalbumName= songsAlbumName.getText().toString().trim();
//                etx_songGenere= songGenere.getText().toString().trim();
//                etxAlbumartistName= songsAlbumArtist.getText().toString().trim();
//                etx_songYear= songYears.getText().toString().trim();
//                etx_songComments= songsComments.getText().toString().trim();
//                Toast.makeText(SongsEditPage.this, "Song saved", Toast.LENGTH_SHORT).show();
//                if(TextUtils.isEmpty(etx_songTitle))
//                {
//                    Toast.makeText(SongsEditPage.this, "Title can't be empty", Toast.LENGTH_SHORT).show();
//                }
//                else if(!TextUtils.isEmpty(etx_songTitle))
//                {
//                    try {
//                       tag.setField(FieldKey.ARTIST,etx_songArtist);
//                        tag.setField(FieldKey.ALBUM_ARTIST,etxAlbumartistName);
//                        tag.setField(FieldKey.TITLE,etx_songTitle);
//                        tag.setField(FieldKey.COMMENT,etx_songComments);
//                        tag.setField(FieldKey.YEAR,etx_songYear);
//                        tag.setField(FieldKey.ALBUM,etx_songalbumName);
//                        tag.setField(FieldKey.GENRE,etx_songGenere);
//
//                    } catch (FieldDataInvalidException e) {
//                        e.printStackTrace();
//                    }
//
//                          audioFile.setTag(tag);
//
//            try {
//                AudioFileIO.write(audioFile);
//                Toast.makeText(SongsEditPage.this, "Tags Edited", Toast.LENGTH_SHORT).show();
//                audioFile.commit();
//                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
//                Toast.makeText(SongsEditPage.this, "new Name"+MediaMetadataRetriever.METADATA_KEY_TITLE, Toast.LENGTH_SHORT).show();
//            }
//            catch (CannotWriteException e)
//            {
//                Log.e("Cant",""+e);
//            }
//
//
//
//
//
//                }
            }
        });

        //   fetch(myPath);

    }

    public void fetch(String path) {
        file = new File(path);

        try {
            audioFile = AudioFileIO.read(file);
            tag = audioFile.getTagOrCreateAndSetDefault();
            //tag.setField(FieldKey.ARTIST,"Demo me");
            title = tag.getFirst(FieldKey.TITLE);
//            byte[] b = tag.getFirstArtwork().getBinaryData();
//            Toast.makeText(this, "artWork"+b, Toast.LENGTH_SHORT).show();
//            Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
//
//            addImageFromGallery.setImageBitmap(bmp);

            if (title.isEmpty()) {
                songName.setText("<unknown title>");
            } else {
                songName.setText(title);
            }

            songTitle.setText(title);
//            audioFile.setTag(tag);
//            try {
//                AudioFileIO.write(audioFile);
//            }
//            catch (CannotWriteException e)
//            {
//                Log.i("Cant",""+e);
//            }
            //          mTitleEditText.setText(title);
            //         mTitleEditText.setSelection(title.length());

            String artist = tag.getFirst(FieldKey.ARTIST);
            songArtist.setText(artist);
            //        mArtistEditText.setSelection(artist.length());

            String album = tag.getFirst(FieldKey.ALBUM);
            songsAlbumName.setText(album);
            //  mAlbumEditText.setSelection(album.length());


            String albumArtist = tag.getFirst(FieldKey.ALBUM_ARTIST);
            songsAlbumArtist.setText(albumArtist);
            // mAlbumArtistEditText.setSelection(albumArtist.length());


            String genre = tag.getFirst(FieldKey.GENRE);
            songGenere.setText(genre);
            //  mGenreEditText.setSelection(genre.length());

            String producer = tag.getFirst(FieldKey.PRODUCER);
            //  mProducerEditText.setText(producer);
            // mProducerEditText.setSelection(mProducerEditText.length());

            String year = tag.getFirst(FieldKey.YEAR);
            songYears.setText(year);
            // mYearEditText.setText(year);
            //  mYearEditText.setSelection(year.length());

            String track = tag.getFirst(FieldKey.TRACK);
            // mTrackEditText.setText(track);
            // mTrackEditText.setSelection(track.length());


            //  String  bitrate = tag.getFirst(FieldKey.DJMIXER);
//            bitRate.setText(bitrate);
            //  mTotalTrackEditText.setText(totalTrack);
            //  mTotalTrackEditText.setSelection(totalTrack.length());

            String totalTrack = tag.getFirst(FieldKey.TRACK_TOTAL);
            //  mTotalTrackEditText.setText(totalTrack);
            //  mTotalTrackEditText.setSelection(totalTrack.length());

            String comment = tag.getFirst(FieldKey.COMMENT);
            //  mCommentsEditText.setText(comment);
            // mCommentsEditText.setSelection(comment.length());
            songsComments.setText(comment);
            //  Log.i("Data ME ", title + "_" + artist + "_" + album + "_" + album + "_" + album + "_" + album);


            String cover = tag.getFirst(FieldKey.COVER_ART);
        //    Toast.makeText(this, "cover is" + cover, Toast.LENGTH_SHORT).show();
            List<Artwork> artwork = tag.getArtworkList();

            if (artwork.size() > 0) {
                byte[] image = artwork.get(0).getBinaryData();
                Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                //    Toast.makeText(this, "bitmap" + bitmap, Toast.LENGTH_SHORT).show();
//                mCardView.setCardBackgroundColor(Palette.from(bitmap).generate().getC(R.color.deep_purple));
                //       mAlbumArtImage.setImageBitmap(bitmap);
            }
        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            e.printStackTrace();
            //  Toast.makeText(getApplicationContext(), "R.string.track_is_malforme", Toast.LENGTH_SHORT).show();
            //finish();
        }

        MediaMetadataRetriever m = new MediaMetadataRetriever();

        m.setDataSource(path);
        // Bitmap thumbnail = m.getFrameAtTime();
//
        if (Build.VERSION.SDK_INT >= 17) {
            String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            String s2 = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
            bitRate.setText(s2.charAt(0) + s2.charAt(1) + s2.charAt(2) + " kbps");
            //     Log.e("Rotation", s + s2);
        }


//        addMetadata(path, METADATA_KEY_ARTIST, "The Rolling Stones");
//        addMetadata(path, METADATA_KEY_ALBUM, "Sticky Fingers");
//        addMetadata(path, METADATA_KEY_TITLE, "Brown Sugar");
//        retriever.setDataSource(path);
//        assertThat(retriever.extractMetadata(METADATA_KEY_ARTIST)).isEqualTo("The Rolling Stones");
//        assertThat(retriever.extractMetadata(METADATA_KEY_ALBUM)).isEqualTo("Sticky Fingers");
//        assertThat(retriever.extractMetadata(METADATA_KEY_TITLE)).isEqualTo("Brown Sugar");


        //       Uri uri= Uri.fromFile(new File(path));

//        ContentValues content = new ContentValues();
//
//        content.put(MediaStore.Audio.Media.ARTIST,
//                "Vijay r");
//
//
//
//        ContentResolver resolver = context.getContentResolver();
//
//        Uri uri= ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,36221);
//
//        resolver.update(uri,content, null,null);

        //  sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 7 && resultCode == RESULT_OK) {
            if (data == null)
                return;
            Uri uri = data.getData();
            try {
                myLoadedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                coverflag = 1;
                MyHelper myHelper = new MyHelper();
                myHelper.execute();
            } catch (Exception e) {

            }


        }
    }

    private byte[] getBytes(InputStream iStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = iStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public String setMp3AlbumArt(File SourceFile, byte[] bytes) {
        String error = null;
        try {
            MP3File musicFile = (MP3File) AudioFileIO.read(SourceFile);
            AbstractID3v2Tag tag = musicFile.getID3v2Tag();
            String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("png")};
            if (tag == null) {
                //      Log.e("TagNull","");
            }

            if (tag != null) {
                Artwork artwork = null;
                try {
                    artwork = ArtworkFactory.createArtworkFromFile(SourceFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    error = e.getMessage();
                }
                if (artwork != null) {
                    //         Log.e("art found","found"+tag.getFields(FieldKey.COVER_ART));
                    artwork.setBinaryData(bytes);
                    tag.deleteArtworkField();
                    artwork.setMimeType(mimeType[0]);
                    tag.setField(artwork);

                    musicFile.setTag(tag);
                    musicFile.commit();
                } else {
                    //       Log.e("art null","art null");
                    artwork.setBinaryData(bytes);
                    tag.addField(artwork);
                    tag.setField(artwork);
                    musicFile.setTag(tag);
                    musicFile.commit();
                }
            }

        } catch (Exception e) {
            error = e.getMessage();
            //    Log.e("Myerror",error);
        }
        return error;
    }


    @Override
    protected void onPause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mediaPlayer.stop();

        }
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mediaPlayer.stop();
        }
    }

//    @Override
//    public void onBackPressed()
//    {
//        if (mediaPlayer != null)
//        {
//            mediaPlayer.pause();
//            mediaPlayer.stop();
//        }
//
//    }

    ////////////////////////////////////////////////////


    void setDetails() {


        for (int i = 0; i < 1; i++) {

            int index = myPath.lastIndexOf('/');

            ArrayList rootDir = getRemovableStorageRoots(getApplicationContext());

            //   Log.e("Root", rootDir + "");

            if (!rootDir.isEmpty() && myPath.contains(rootDir.get(0).toString())) {

                sdcardStorage = rootDir.get(0).toString();
                //   Log.e("FLag","Got here");
                moveFile(i, myPath.substring(0, index + 1), myPath.toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/");
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(myPath + ""))));


            } else {

                //        moveFile(pathList.get(i).toString().substring(0, index + 1), pathList.get(i).toString().substring(index + 1), Environment.getExternalStorageDirectory() + "/");


                //   Uri uri= Uri.parse("file:///"+ pathList.get(0));

                File file = new File(myPath + "");

//            File file = new File(uri+"");

                if (coverflag == 0) {

                    //   Log.e("FLag","Got here if");

                    if (songTitle.getText().toString().equals("")) {
                        til1.setError("Empty field");
                        songTitle.setFocusable(true);
                        return;
                    }
                    if (songArtist.getText().toString().equals("")) {
                        til2.setError("Empty field");
                        songArtist.setFocusable(true);
                        return;
                    }
                    if (songGenere.getText().toString().equals("")) {
                        til3.setError("Empty field");
                        songGenere.setFocusable(true);
                        return;
                    }
//                    if (songsLyrics.getText().toString().equals("")) {
//                        tile4.setError("Empty field");
//                        songsLyrics.setFocusable(true);
//                        return;
//                    }
                    if (songsComments.getText().toString().equals("")) {
                        tile5.setError("Empty field");
                        songsComments.setFocusable(true);
                        return;
                    }
                    if (songsAlbumName.getText().toString().equals("")) {
                        tile6.setError("Empty field");
                        songsAlbumName.setFocusable(true);
                        return;
                    }
                    if (songsAlbumArtist.getText().toString().equals("")) {
                        tile7.setError("Empty field");
                        songsAlbumArtist.setFocusable(true);
                        return;
                    }
                    if (songYears.getText().toString().equals("")) {
                        tile8.setError("Empty field");
                        songYears.setFocusable(true);
                        return;
                    }


                    try {


                        AudioFile audioFile = AudioFileIO.read(file);

                        Tag tag = audioFile.getTagOrCreateAndSetDefault();

                        tag.setField(FieldKey.TITLE, songTitle.getText().toString() + "");

                        tag.setField(FieldKey.ALBUM_ARTIST, songsAlbumArtist.getText().toString() + "");

                        tag.setField(FieldKey.YEAR, songYears.getText().toString() + "");

                        tag.setField(FieldKey.GENRE, songGenere.getText().toString() + "");
                        tag.setField(FieldKey.COMMENT, songsComments.getText().toString() + "");
                        tag.setField(FieldKey.ARTIST, songArtist.getText().toString() + "");
                        tag.setField(FieldKey.ALBUM, songsAlbumName.getText().toString() + "");


                        audioFile.setTag(tag);

                        audioFile.commit();
                        try {
                            AudioFileIO.write(audioFile);

                        } catch (CannotWriteException e) {
                            //       Log.i("Cant", "" + e);
                        }


                    } catch (Exception e) {

                    }
                } else {

                    //  Log.e("Local file", "");

                    Bitmap tmbitmap = myLoadedBitmap;

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    // myLoadedBitmap.recycle();
                    //       Log.e("Local file byte", "" + byteArray);

//
//                    Uri imageUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id));
//                    Bitmap scaledbmp = decodeSampledBitmapFromResource(imageUri, 300, 300);
//                    if (scaledbmp != null) {
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        scaledbmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] byteArray = stream.toByteArray();
//

                    setMp3AlbumArt(file, byteArray);
                }

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(myPath + ""))));
            }

        }


    }

    public void moveFile(int myRelIndex, String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                //      Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();

        } catch (FileNotFoundException fnfe1) {
            //      Log.e("tagg", fnfe1.getMessage());
            //      Toast.makeText(getApplicationContext(), "Something went Wrong!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            //      Log.e("tag", e.getMessage());
            //     Toast.makeText(getApplicationContext(), "Something went Wrong!!", Toast.LENGTH_SHORT).show();
        }

        File file = new File(outputPath + "/" + inputFile);

        if (coverflag == 0) {
            //     Log.e("FLag","Got here too");


            //      Log.e("Field","arrived here");

            try {

                AudioFile audioFile = AudioFileIO.read(file);

                Tag tag = audioFile.getTagOrCreateAndSetDefault();

                tag.setField(FieldKey.TITLE, songTitle.getText().toString() + "");

                tag.setField(FieldKey.ALBUM_ARTIST, songsAlbumArtist.getText().toString() + "");

                tag.setField(FieldKey.YEAR, songYears.getText().toString() + "");

                tag.setField(FieldKey.GENRE, songGenere.getText().toString() + "");
                tag.setField(FieldKey.COMMENT, songsComments.getText().toString() + "");
                tag.setField(FieldKey.ARTIST, songArtist.getText().toString() + "");
                tag.setField(FieldKey.ALBUM, songsAlbumName.getText().toString() + "");


                audioFile.setTag(tag);

                audioFile.commit();
                try {
                    AudioFileIO.write(audioFile);

                } catch (CannotWriteException e) {
                    //       Log.i("Cant", "" + e);
                }


            } catch (Exception e) {
                //      Log.e("Excepppppppp",e.getMessage()+"");
            }
        } else {

            Bitmap tmbitmap = myLoadedBitmap;

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            tmbitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            // myLoadedBitmap.recycle();


//
//                    Uri imageUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id));
//                    Bitmap scaledbmp = decodeSampledBitmapFromResource(imageUri, 300, 300);
//                    if (scaledbmp != null) {
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        scaledbmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] byteArray = stream.toByteArray();
//

            setMp3AlbumArt(file, byteArray);
        }
        //  String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

        SharedPreferences sharedPreferences = getSharedPreferences("saf", MODE_PRIVATE);
        String mySafUri = sharedPreferences.getString("saf", "");

//                ArrayList rootS=getRemovableStorageRoots(getApplicationContext());
//
//                StringBuilder sb=new StringBuilder("");
//
//                String comtext=inputPath+inputFile;
//                int comindex=comtext.charAt('/');
//
//                String myFinalPath=(inputPath).substring(rootS.get(0).toString().length()+1);
//
//                for(int i=0;i<myFinalPath.length()-1;i++)
//                {
//                    if(myFinalPath.charAt(i)=='/')
//                    {
//                        sb.append("%2F");
//                    }
//                    else if(myFinalPath.charAt(i)==' ')
//                    {
//                        sb.append("%20");
//                    }
//                    else if(myFinalPath.charAt(i)=='+')
//                    {
//                        sb.append("%2B");
//                    }
//                    else if(myFinalPath.charAt(i)=='*')
//                    {
//                        sb.append("%C3%B7");
//                    }
//                    else if(myFinalPath.charAt(i)=='=')
//                    {
//                        sb.append("%3D");
//                    }
//                    else if(myFinalPath.charAt(i)=='%')
//                    {
//                        sb.append("%25");
//                    }
//                    else if(myFinalPath.charAt(i)=='&')
//                    {
//                        sb.append("%26");
//                    }
//                    else if(myFinalPath.charAt(i)=='@')
//                    {
//                        sb.append("%40");
//                    }
//                    else if(myFinalPath.charAt(i)=='$')
//                    {
//                        sb.append("%24");
//                    }
//                    else if(myFinalPath.charAt(i)=='#')
//                    {
//                        sb.append("%23");
//                    }
//                    else if(myFinalPath.charAt(i)=='₹')
//                    {
//                        sb.append("%E2%82%B9");
//                    }
//                    else
//                    {
//                        sb.append(myFinalPath.charAt(i));
//                    }
//                }
//
//                String pathg=mySafUri+sb.toString();
//
//                Log.e("MyFinalPath",myFinalPath);
//                Log.e("Roots",rootS+"");
//                Log.e("MySaf",mySafUri+"");
//                Log.e("My Sb",sb+"");
//                Log.e("My inputfilee",inputFile+"");
//                Log.e("MyConvSAF",mySafUri+sb.toString());

        //      grantUriPermission(getPackageName(), Uri.parse(mySafUri), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );

//                grantUriPermission(getPackageName(), Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                final int takeFlags =   (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                getContentResolver().takePersistableUriPermission(Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3AMusic"), takeFlags);

//                        DocumentFile pickedDirr = DocumentFile.fromTreeUri(getApplicationContext(), Uri.parse("/3863-6134/audio/media/70052"));
//content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
        // DocumentFile pickedDir = pickedDirr.createFile(mimeType[0], inputFile);

        DocumentFile pickedDirr = findMySafDir(inputPath.substring(sdcardStorage.length()) + "", Uri.parse(mySafUri), inputFile);

        moveFileBackBySaf(outputPath, inputFile, pickedDirr);


        //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit
        //content://com.android.externalstorage.documents/tree/3863-6134%3ASHAREit%20%20(1)%2Ffiles%2FSHAREit_Vjj%2Faudios%2Fshareit

//                SafHelper safHelper=new SafHelper(getApplicationContext(),"/storage/3863-6134/",1);
//                safHelper.requestPermissions(Song_Edits.this);

//            } catch (Exception e) {
//                Log.e("errrrror", e + "");
//            }
//
//

        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(myPath + ""))));

    }


    private void moveFileBackBySaf(String inputPath, String inputFile, DocumentFile sdDir) {

        //  Log.e("Moving file back", "" + inputFile);

        //  Log.e("inputpath", "" + inputPath);
        //  Log.e("inputfile", "" + inputFile);
        //   Log.e("inputfile", "" + completePath);
        //     Log.e("outputpath",""+outputPath);

        InputStream in = null;
        OutputStream out = null;
        try {

            String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3");
            //  Log.e("mime:", mimeType);

//            //create output directory if it doesn't exist
//            File dir = new File (outputPath);
//            if (!sdDir.exists())
//            {
//                sdDir.getParentFile().createFile(mimeType,"newfile.mp3");
//            }


            in = new FileInputStream(inputPath + inputFile);


            out = getApplicationContext().getContentResolver().openOutputStream(sdDir.getUri());

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
                //   Log.e("buffer", buffer + "");
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();


        } catch (FileNotFoundException fnfe1) {
            //    Log.e("tagg", fnfe1.getMessage());
        } catch (Exception e) {
            //   Log.e("tag", e.getMessage());
        }

    }

    public static ArrayList getRemovableStorageRoots(Context context) {
        File[] roots = context.getExternalFilesDirs("external");
        ArrayList<File> rootsArrayList = new ArrayList<>();
        for (int i = 0; i < roots.length; i++) {
            if (roots[i] != null) {
                String path = roots[i].getPath();
                int index = path.lastIndexOf("/Android/data/");
                if (index > 0) {
                    path = path.substring(0, index);
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath())) {
                        rootsArrayList.add(new File(path));
                    }
                }
            }
        }

        //    Log.e("MyPaths", rootsArrayList + "");
//
//        roots = new File[rootsArrayList.size()];
//        rootsArrayList.toArray(roots);
        return rootsArrayList;
    }

    DocumentFile findMySafDir(String relative_path, Uri uri, String filename) {

        ArrayList pathFragments = new ArrayList();

        StringBuilder sb = new StringBuilder("");

        for (int i = 0; i < relative_path.length(); i++) {
            if (relative_path.charAt(i) == '/') {
                pathFragments.add(sb + "");
                sb = new StringBuilder("");
            } else {
                sb.append(relative_path.charAt(i));
            }
        }

        DocumentFile documentFile = DocumentFile.fromTreeUri(getApplicationContext(), uri);
        //  DocumentFile[] docs=documentFile.listFiles();
        DocumentFile[] docs;
        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};

        String pathUnit = "";
        for (int j = 0; j < pathFragments.size(); j++) {
            pathUnit = pathFragments.get(j) + "";

            docs = documentFile.listFiles();

            for (int i = 0; i < docs.length; i++) {
                if (docs[i].getName().toString().equals(pathUnit)) {
                    documentFile = docs[i];
                    //    Log.e("Done at", "" + pathUnit);
                    break;
                } else {
                    //   Log.e("FinderError", "Lg gye at" + pathUnit);
                }
            }

        }


        DocumentFile ret_doc_fi = documentFile.findFile(filename);

        ret_doc_fi.delete();

        DocumentFile ret_doc_file = documentFile.createFile(mimeType[0], filename);

        return ret_doc_file;
    }

    class MyHelper extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            setDetails();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //     Toast.makeText(getApplicationContext(), "Details Saved", Toast.LENGTH_SHORT).show();

            Glide.with(getApplicationContext()).load(R.drawable.check).into(gifview);

            progressBar.setVisibility(View.INVISIBLE);
            gifview.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {


                    try {
                        Thread.sleep(2500);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressDialog.setVisibility(View.GONE);
                                onBackPressed();
                            }
                        });


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }
    }


    @Override
    public void onBackPressed() {

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra("data", "songgg");
        startActivity(i);
        finish();
    }

    public void playPauseSong() {
        try {


            if (flag == 0) {

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(myPath);
                mediaPlayer.prepare();
                mediaPlayer.start();
                flag = 1;
                More_icon2.setBackgroundResource(R.drawable.player_pause);

            } else if (flag == 1) {


                mediaPlayer.stop();
                More_icon2.setBackgroundResource(R.drawable.player_playt);
                flag = 0;


            }


        } catch (Exception e) {
            //        Toast.makeText(this, "exception" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}