package com.music.detail.changer.Fragments.SongAdapters;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.music.detail.changer.MainActivity;
import com.music.detail.changer.R;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    public Context context;
    public ArrayList title, cover, artist, pathList;
    int flag = 0;
    LinearLayout linearLayout;
    FloatingActionButton floatingActionButton;
    public ArrayList clicks;
    Toolbar toolbar;
    TextView numbsSongsSelectd;
    public ImageView bacFromSelected;
    public ArrayList selectedPathList;

    public SongAdapter(Context context, ArrayList title, ArrayList cover, ArrayList artist, ArrayList pathList,
                       LinearLayout recyclerSelectToolbarLayout, FloatingActionButton floatingActionButton, TextView numbsItemsSelected, Toolbar toolbar) {
        this.context = context;
        this.title = title;
        this.cover = cover;
        this.artist = artist;
        this.linearLayout = recyclerSelectToolbarLayout;
        clicks = new ArrayList();
        this.floatingActionButton = floatingActionButton;
        this.numbsSongsSelectd = numbsItemsSelected;
        this.pathList = pathList;
        this.toolbar = toolbar;
        selectedPathList = new ArrayList();
    }

    @NonNull
    @Override
    public SongAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_songs_design, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull final SongAdapter.ViewHolder holder, final int position) {

        bacFromSelected = linearLayout.findViewById(R.id.backFromSelectedItems);
        holder.songName.setText((title.get(position) + ""));
        holder.songDetail.setText((artist.get(position) + ""));
//         bacFromSelected.setOnClickListener(new View.OnClickListener() {
//             @Override
//             public void onClick(View v) {
//
//             }
//         });

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), getImage(Long.parseLong(cover.get(position).toString())));

            holder.songImage.setImageBitmap(bitmap);
        } catch (Exception e) {

            holder.songImage.setImageResource(R.drawable.songs_icon);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {


                if (flag == 0) {

                    Intent intent = new Intent(context, SongsEditPage.class);
                    intent.putExtra("songs", pathList.get(position) + "");
                    // intent.putExtra("cover",cover.get(position)+"");
                    context.startActivity(intent);
                    ((MainActivity) context).finish();
                } else {
                    if (!clicks.contains(position)) {


                        if (clicks.size() < 3) {

                            clicks.add(position);
                            selectedPathList.add(pathList.get(position));
                            numbsSongsSelectd.setText(clicks.size() + " Selected");

                            holder.view.setBackgroundColor(Color.LTGRAY);
                            holder.songImage.setBackgroundColor(R.drawable.album_icon);

                        } else {
                            Toast.makeText(context, "Ony Three Songs At Once", Toast.LENGTH_SHORT).show();
                            flag = 2;

                        }

                    } else {
                        int index = clicks.indexOf(position);
                        clicks.remove(index);
                        holder.view.setBackgroundColor(Color.WHITE);
                        selectedPathList.remove(pathList.get(position));
                        if (clicks.size() > 0) {

                            numbsSongsSelectd.setText(clicks.size() + " Selected");
                        }


                    }

                    if (clicks.isEmpty()) {
                        flag = 0;
                        linearLayout.setVisibility(View.INVISIBLE);
                        toolbar.setVisibility(View.VISIBLE);
                        ((MainActivity) context).flag = 0;
                        floatingActionButton.setVisibility(View.INVISIBLE);
                        holder.view.setBackgroundColor(Color.WHITE);
                    }

                }

            }
        });
        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ((MainActivity) context).flag = 0;

                if (flag == 2) {
                    //
                } else {
                    flag = 1;
                    linearLayout.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.INVISIBLE);
                    floatingActionButton.setVisibility(View.VISIBLE);
                    if (!clicks.contains(position)) {

                        clicks.add(position);
                        selectedPathList.add(pathList.get(position));
                        numbsSongsSelectd.setText(clicks.size() + " Selected");

                        holder.view.setBackgroundColor(Color.LTGRAY);
                    }
                }


                return true;
            }


        });

    }

    private Uri getImage(long albumId) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId);
    }

    @Override

    public int getItemCount() {
        return title.size();
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public String getSectionName(int position) {

        return (title.get(position).toString().substring(0, 1));//getNameForItem(position);//String.format("%d", position + 1);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView songName, songDetail;
        ImageView songImage;
        private View view;

        public ViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            songName = itemView.findViewById(R.id.song_name);
            songDetail = itemView.findViewById(R.id.song_detail_or_singerName);
            songImage = itemView.findViewById(R.id.song_image);
            view = itemView.findViewById(R.id.designView);
        }


    }

    public void setFilter(ArrayList songg, ArrayList artistt, ArrayList coverr, ArrayList pathlist) {
        this.title = songg;
        this.artist = artistt;
        this.cover = coverr;
        this.pathList = pathlist;
        notifyDataSetChanged();
    }


}
