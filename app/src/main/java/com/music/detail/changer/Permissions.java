package com.music.detail.changer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.music.detail.changer.Fragments.SongAdapters.SongLoader;

import java.io.File;
import java.util.ArrayList;

import ru.bartwell.safhelper.SafHelper;

import static android.os.FileUtils.closeQuietly;

public class Permissions extends AppCompatActivity {
    TextView normalstatus, safstatus, notice;
    Button normalrequest, safrequest, proceed;
    int flag = 0;
    int temp = 0;
    ArrayList arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        //   getSupportActionBar().hide();

        normalstatus = findViewById(R.id.permission_text_status);
        normalrequest = findViewById(R.id.permission_request_button);

        safstatus = findViewById(R.id.sdcardpermission_text_status);
        safrequest = findViewById(R.id.sdcardpermission_request_button);

        notice = findViewById(R.id.permission_text_notice);

        proceed = findViewById(R.id.permission_proceed_button);

        proceed.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        SongLoader songLoader = new SongLoader();

    //    Log.e("Pran mere", songLoader.getMyIDByDate(getApplicationContext()) + "");

        arrayList = getRemovableStorageRoots(getApplicationContext());

        if (arrayList.isEmpty()) {
            safrequest.setVisibility(View.GONE);
            safstatus.setVisibility(View.GONE);
            flag = 1;
        }


        if (permissionGranted() && flag == 1) {
            temp = 1;
            normalstatus.setText("Accepted");
            normalstatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_green, 0, 0, 0);
            //   request.setVisibility(View.GONE);
            notice.setVisibility(View.GONE);
            proceed.setVisibility(View.VISIBLE);
        }


        normalrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
            }
        });

        safrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SafHelper safHelper=new SafHelper(getApplicationContext(),""+arrayList.get(0),7);

                safHelper.requestPermissions(Permissions.this);

//                File file = new File(arrayList.get(0)+"");
//
//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
//                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Uri.parse("content://com.android.externalstorage.documents/tree/3863-6134%3A"));
//                intent.addFlags(
//                        Intent.FLAG_GRANT_READ_URI_PERMISSION
//                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
//                );
//                startActivityForResult(intent, 7);

            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("perm", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("perm", "done");
                editor.commit();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("data", "so");
                startActivity(i);
                finish();

            }
        });

    }

    private boolean permissionGranted() {
        return ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(Permissions.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.MANAGE_DOCUMENTS}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            normalstatus.setText("Accepted");
            normalstatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_green, 0, 0, 0);
            //  request.setVisibility(View.GONE);
            //  notice.setVisibility(View.GONE);
            temp = 1;
            if (flag == 1) {
                proceed.setVisibility(View.VISIBLE);
            }
        }
    }

    public static ArrayList getRemovableStorageRoots(Context context) {
        File[] roots = context.getExternalFilesDirs("external");
        ArrayList<File> rootsArrayList = new ArrayList<>();
        for (int i = 0; i < roots.length; i++) {
            if (roots[i] != null) {
                String path = roots[i].getPath();
                int index = path.lastIndexOf("/Android/data/");
                if (index > 0) {
                    path = path.substring(0, index);
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath())) {
                        rootsArrayList.add(new File(path));
                    }
                }
            }
        }

//
//        roots = new File[rootsArrayList.size()];
//        rootsArrayList.toArray(roots);
        return rootsArrayList;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 7) {
            if (data == null)
                return;


            SharedPreferences sharedPreferences = getSharedPreferences("saf", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("saf", data.getData() + "");
            editor.commit();

            safstatus.setText("Accepted");
            safstatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_green, 0, 0, 0);

            flag = 1;

            if (temp == 1) {
                proceed.setVisibility(View.VISIBLE);
            }

//            DocumentFile pickedDir = DocumentFile.fromTreeUri(getApplicationContext(), data.getData());
            grantUriPermission(getPackageName(), data.getData(), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            getContentResolver().takePersistableUriPermission(data.getData(), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

//          E/MyData: content://com.android.externalstorage.documents/tree/3863-6134%3A
//          E/PickedDir: androidx.documentfile.provider.TreeDocumentFile@891221d

        }

    }

    public Uri prepareTreeUri(Uri treeUri) {
        String documentId;

        DocumentFile documentFile = DocumentFile.fromTreeUri(getApplicationContext(), treeUri);
        DocumentFile[] docs = documentFile.listFiles();
        int i = 0;
        String mimeType[] = {android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3")};
        for (DocumentFile d : docs) {
            i++;

            if (i == 5) {
                d.createFile(mimeType[0], "lol.mp3");
            }
        }


        try {
            documentId = DocumentsContract.getDocumentId(treeUri);
            if (documentId == null) {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            // IllegalArgumentException will be raised
            // if DocumentsContract.getDocumentId() failed.
            // But it isn't mentioned the document,
            // catch all kinds of Exception for safety.
            documentId = DocumentsContract.getTreeDocumentId(treeUri);
        }
        return DocumentsContract.buildDocumentUriUsingTree(treeUri, documentId);
    }


}